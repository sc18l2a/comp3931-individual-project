'''
create all data files used by spellChecker, checkSpell and checkGrammar modules
'''
import xml.sax
import os
import os.path
import pickle
import json 
import shelve

#link to download BNC:
#https://ota.bodleian.ox.ac.uk/repository/xmlui/handle/20.500.12024/2554

class bncHandler( xml.sax.ContentHandler ):
    def __init__(self, wordList, unigramPOSList, bigramWordList, bigramPOSList, trigramWordList, trigramPOSList, quadgramWordList, quadgramPOSList):
        self.unigramWordList=wordList #all possible words, to be updated with frequencey from processed corpus
        self.unigramPOSList=unigramPOSList #store for all POS tags from processed corpus
        self.bigramWordList=bigramWordList #store for all sets of two consecutive valid words from processed corpus
        self.bigramPOSList=bigramPOSList #store for all sets of two consecutive valid words, with the first as POS tag, from processed corpus
        self.trigramWordList=trigramWordList #store for all sets of three consecutive valid words from processed corpus
        self.trigramPOSList=trigramPOSList #store for all sets of three consecutive valid words, with the first two as POS tag, from processed corpus
        self.quadgramWordList=quadgramWordList #store for all sets of four consecutive valid words from processed corpus
        self.quadgramPOSList=quadgramPOSList #store for all sets of four consecutive valid words, with the first three as POS tag, from processed corpus
        self.unigramWordList["!SentenceCount!"]=[0,""]
        self.wordFound=False
        self.newWord=""
        #variables that record the last four words processed
        self.word=""
        self.previousWord1=""
        self.previousWord2=""
        self.previousWord3=""

    def startElement(self, tag, attributes):
        if (tag=="w"):
            self.wordFound=True
            if ("c5" in attributes):
                self.wordType=attributes["c5"]
            else:
                self.wordType=""

    def endElement(self, tag):
        if (tag=="s"):
            #end of sentance found, clear word history used for ngrams
            self.unigramWordList["!SentenceCount!"][0]+=1
            self.previousWord3 = ""
            self.previousWord2 = ""
            self.previousWord1 = ""
            self.word = ""
        elif (tag=="w" and self.wordFound):
            #prepare newWord
            self.newWord=self.newWord.strip().lower()
            if (self.newWord not in self.unigramWordList):
                #if the word is not in list of known words, set it to blank
                #blanks will be stored as "" in the corpus with associated frequency
                #This will cause ngram creation to be skipped
                self.newWord=""
            #move record of previous words on by one
            self.previousWord3 = self.previousWord2
            self.previousWord2 = self.previousWord1
            self.previousWord1 = self.word
            self.word = self.newWord
            #now do word store to main dictionary
            if (self.unigramWordList[self.word][0]==0):#if first time word found in corpus, assign list [1, wordType]
                self.unigramWordList[self.word]=[1, self.wordType]
            else:#else just increment the frequency count as no need to update wordtype every time
                self.unigramWordList[self.word][0]=self.unigramWordList[self.word][0]+1

            #now do quadgram store - start with key made from last four words
            if (self.quadgramWordList!=None and self.previousWord3!="" and self.previousWord2!="" and self.previousWord1!="" and self.word!=""):
                ngram = self.previousWord3+","+self.previousWord2+","+self.previousWord1+","+self.word
                if(ngram in self.quadgramWordList):
                    #if already stored, increment frequency count
                    currentFrequency=self.quadgramWordList[ngram][0]
                    self.quadgramWordList[ngram] = [currentFrequency+1, 0]
                else:
                    #else assign list [1,0] which is for [frequency, probability]
                    self.quadgramWordList[ngram]=[1,0]

            #now do quadgram POS store - start with key made from last four words
            if (self.quadgramPOSList!=None and self.previousWord3!="" and self.previousWord2!="" and self.previousWord1!="" and self.word!=""):
                ngram = self.unigramWordList[self.previousWord3][1]+","+self.previousWord2+","+self.unigramWordList[self.previousWord1][1]+","+self.word
                if(ngram in self.quadgramPOSList):
                    #if already stored, increment frequency count
                    currentFrequency=self.quadgramPOSList[ngram][0]
                    self.quadgramPOSList[ngram] = [currentFrequency+1, 0]
                else:
                    #else assign list [1,0] which is for [frequency, probability]
                    self.quadgramPOSList[ngram]=[1,0]

            #now do trigram store - start with key made from last three words
            if (self.trigramWordList!=None and self.previousWord2!="" and self.previousWord1!="" and self.word!=""):
                trigram = self.previousWord2+","+self.previousWord1+","+self.word
                if(trigram in self.trigramWordList):
                    #if trigram already stored, increment frequency count
                    currentFrequency=self.trigramWordList[trigram][0]
                    self.trigramWordList[trigram] = [currentFrequency+1, 0]
                else:
                    #else assign list [1,0] which is for [frequency, probability]
                    self.trigramWordList[trigram]=[1,0]

            #now do trigram POS store - start with key made from last three words
            if (self.trigramPOSList!=None and self.previousWord2!="" and self.previousWord1!="" and self.word!=""):
                trigram = self.unigramWordList[self.previousWord2][1]+","+self.previousWord1+","+self.unigramWordList[self.word][1]
                if(trigram in self.trigramPOSList):
                    #if trigram already stored, increment frequency count
                    currentFrequency=self.trigramPOSList[trigram][0]
                    self.trigramPOSList[trigram] = [currentFrequency+1, 0]
                else:
                    #else assign list [1,0] which is for [frequency, probability]
                    self.trigramPOSList[trigram]=[1,0]

            #next do bigram store - start with key made from last two words
            if (self.bigramWordList!=None and self.previousWord1!="" and self.word!=""):
                bigram = self.previousWord1+","+self.word
                if(bigram in self.bigramWordList):
                    currentFrequency=self.bigramWordList[bigram][0]
                    self.bigramWordList[bigram] = [currentFrequency+1, 0]
                else:
                    self.bigramWordList[bigram]=[1,0]

            #next do bigram POS store - start with key made from last two words
            if (self.bigramPOSList!=None and self.previousWord1!="" and self.word!=""):
                bigram = self.unigramWordList[self.previousWord1][1]+","+self.word
                if(bigram in self.bigramPOSList):
                    currentFrequency=self.bigramPOSList[bigram][0]
                    self.bigramPOSList[bigram] = [currentFrequency+1, 0]
                else:
                    self.bigramPOSList[bigram]=[1,0]

            #next to unigram POS store
            if (self.unigramPOSList!=None and self.word!=""):
                unigram = self.unigramWordList[self.word][1]
                if (unigram in self.unigramPOSList):
                    currentFrequency=self.unigramPOSList[unigram][0]
                    self.unigramPOSList[unigram] = [currentFrequency+1, 0]
                else:
                    self.unigramPOSList[unigram]=[1,0]

            self.wordFound=False
            self.newWord=""

    def characters(self, content):
        if (self.wordFound==True):
            self.newWord+=content




#load known words from file (for example https://github.com/dwyl/english-words/)
#create dictionary to store the processed corpus
f = open('words_dictionary2.json',) 
wordList=json.load(f)


unigramPOSList= shelve.open("Dict1gramPOS.db")
#unigramPOSList=None

bigramWordList = shelve.open("Dict2gram.db")
#bigramWordList=None

bigramPOSList = shelve.open("Dict2gramPOS.db")
#bigramPOSList=None

trigramWordList = shelve.open("Dict3gram.db")
#trigramWordList=None

trigramPOSList = shelve.open("Dict3gramPOS.db")
#trigramPOSList=None

quadgramWordList = shelve.open("Dict4gram.db")
#quadgramWordList=None

quadgramPOSList = shelve.open("Dict4gramPOS.db")
#quadgramPOSList=None


#create xml parser
parser = xml.sax.make_parser()
parser.setFeature(xml.sax.handler.feature_namespaces, 0)
Handler = bncHandler(wordList, soundexList, unigramPOSList, bigramWordList, bigramPOSList, trigramWordList, trigramPOSList, quadgramWordList, quadgramPOSList) #create object that will do the work collating words
parser.setContentHandler(Handler)
#now load and parse the British National Corpus
for dirpath, dirnames, filenames in os.walk("bnc"): #for every file in bnc dir (including subdirs)
    for filename in [f for f in filenames if f.endswith(".xml")]: #only process xml files
        print(os.path.join(dirpath, filename))
        parser.parse(os.path.join(dirpath, filename))


####POST PROCESSING
#remove words of 0 freq from the main dict, because they were not found in the corpus
shorterWordList={}
for word in wordList:
    if (wordList[word][0] > 0):
        shorterWordList[word]=wordList[word]
wordList=shorterWordList

#sum the word frequencies
wordFrequencyTotal=0
for wordAttr in wordList.values():
    wordFrequencyTotal=wordFrequencyTotal+wordAttr[0]
wordList["WordFrequencyTotal"]=[wordFrequencyTotal,""]
#store average sentence length (using BNC sentence count stored in wordList during XML parsing)
wordList["AverageSentenceLength"] = [wordFrequencyTotal/wordList["!SentenceCount!"][0], ""]

#remove 4grams with freq 1, because they are uncommon
##start by getting 4gram total
if (quadgramWordList != None):
    quadgramFrequencyTotal=0
    for attr in quadgramWordList.values():
        quadgramFrequencyTotal+=attr[0]
    ##remove freq 1 grams
    quadgramWordListShort = shelve.open("Dict4gramShort.db")
    for gram in quadgramWordList:
        if (quadgramWordList[gram][0] > 1):
            quadgramWordListShort[gram] = quadgramWordList[gram]
    quadgramWordList.close()
    quadgramWordList=None
    os.remove("Dict4gram.db")
    ##calculate the lowest 4gram probability and store in ngram dict
    quadgramWordListShort["LowestProbability"] = 1.0 / quadgramFrequencyTotal

#remove 3grams with freq 1, because they are uncommon
##start by getting 4gram total
if (trigramWordList != None):
    trigramFrequencyTotal=0
    for attr in trigramWordList.values():
        trigramFrequencyTotal+=attr[0]
    ##remove freq 1 grams
    trigramWordListShort = shelve.open("Dict3gramShort.db")
    for gram in trigramWordList:
        if (trigramWordList[gram][0] > 1):
            trigramWordListShort[gram] = trigramWordList[gram]
    trigramWordList.close()
    trigramWordList=None
    os.remove("Dict3gram.db")
    ##calculate the lowest 4gram probability and store in ngram dict
    trigramWordListShort["LowestProbability"] = 1.0 / trigramFrequencyTotal


#close disk based Dictionaries
if (quadgramWordListShort):
    quadgramWordListShort.close()
if (quadgramPOSList):
    quadgramPOSList.close()
if (trigramWordList):
    trigramWordList.close()
if (trigramPOSList):
    trigramPOSList.close()
if (bigramWordList):
    bigramWordList.close()
if (bigramPOSList):
    bigramPOSList.close()
if (unigramPOSList):
    unigramPOSList.close()

#write in memory Dictionary to disk
with open('Dict1gram.json', 'w') as filehandle:
    json.dump(wordList, filehandle)


