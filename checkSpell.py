import re
#from collections import Counter
#from urllib.request import urlopen
#import operator
import numpy as np
import json
import copy

from functools import lru_cache

#opens basic word (unigram) dictionary for use by "known" and "is_spelled_correctly and "frequency_probability"
with open('Dict1gram.json', 'r') as filehandle:
    WORDS = json.load(filehandle)


#Constants refer to the array positions of the frequency and c5 tag stored with each word in the dictionary.
WORD_FREQ = 0
WORD_POS = 1


#Flag to make the module language independent, applys only P2 values to any candidates if set False, P2 relies entirely on simple data from a corpus.
#Can be any language providing a corpus can be parsed.
ENGLISH = True

'''
Module description

This is the spellchecking module, called from the user interface when a word that can't be found in the dictionary is typed.  
Used but the grammar checking module to populate confusion sets of known words

'''


#For grammar check candidates - pre defined confusion sets, words that could be confused with one another, but arent within 2 edits from eachother.
#Limited implementation as not language independant (specific to english words)
_CONFUSION_SET_ADDITIONS = [['imply','infer'],
                            ['fewer','less'],
                            ['much','many'],
                            ['among','between'],
                            ['disinterested','uninterested'],
                            ['say','tell'],
                            ['peek','pique'],
                            ['are','is']]


#For collected words function
_collected_words = set()

def register_collected_words(collected_word_set):
    '''receive list of words that should give priority when ordering the candidates list.'''
    global _collected_words
    _collected_words = collected_word_set


def is_spelled_correctly(word):
    '''check if a typed word is spelt correctly'''
    if (word.strip().lower().replace("'", "") in WORDS):
        return True
    else:
        return False



def candidates(word):
    '''returns a list of, at most, the top 10 most likely words that are dictionary words up to 2 edits from the "word" parameter '''
    global _collected_words
    ranked_list = []
    word_copy = word.strip().lower().replace("'", "")
    new_dict = {**candidates_1_delete(word_copy),**candidates_1_transpose(word_copy),
                **candidates_1_insert(word_copy),**candidates_1_substitute(word_copy),
                **candidates_2_ss(word_copy),**candidates_2_tt(word_copy),**candidates_2_ii(word_copy),
                **candidates_2_dd(word_copy),**candidates_2_st(word_copy),
                **candidates_2_is(word_copy),
                **candidates_2_sd(word_copy),**candidates_2_ds(word_copy),
                **candidates_2_ti(word_copy),**candidates_2_it(word_copy),
                **candidates_2_td(word_copy),**candidates_2_dt(word_copy),
                **candidates_2_di(word_copy)}
    new_dict = normalise(new_dict)
    for key in sorted(new_dict.items(), key=lambda x: x[1], reverse=True):
        if (key[0] in _collected_words):
            ranked_list.insert(0, key[0]) #add collected word to top of list
        else:
            ranked_list.append(key[0])    #add generated word to bottom of list
    return ranked_list[0:10]               #only return the top 10 most likely words



def candidates_grammar_check(word):
    '''Return list of 20 most probable words within 2 edits or in _CONFUSION_SET_ADDITIONS based on "word" parameter (ranked by frequency (P2)). 
       Filter so returned words have same first letter of POS tag as the word'''
    word_copy = word
    allCandidatesDict = {}
    topRankedList=[]
    confusedWord = find_confused_word(word_copy)
    allCandidates = set((known(edits_1_grammar_delete(word_copy)) + known(edits_1_transpose(word_copy)) +
                         known(edits_1_grammar_insert(word_copy)) + known(edits_1_grammar_substitute(word_copy)) +
                         known(edits_2_dd(word_copy)) + known(edits_2_ss(word_copy)) +
                         known(edits_2_tt(word_copy)) + known(edits_2_ii(word_copy)) +
                         known(edits_2_sd(word_copy)) + known(edits_2_ds(word_copy)) + 
                         known(edits_2_ti(word_copy)) + known(edits_2_it(word_copy)) + 
                         known(edits_2_td(word_copy)) + known(edits_2_dt(word_copy)) + 
                         known(edits_2_di(word_copy)) + known(edits_2_st(word_copy)) + known(edits_2_is(word_copy))))
    listAllCandidates = list(allCandidates)
    for item in listAllCandidates:
        if (WORDS[word][WORD_POS][:0]==WORDS[item][WORD_POS][:0]):
            allCandidatesDict[item] = frequency_probability(item)
    allRankedTuples = sorted(allCandidatesDict.items(), key=lambda x: x[1], reverse=True)
    for key in allRankedTuples:
        topRankedList.append(key[0])
    cutTopRankedList = topRankedList[0:20]
    if (confusedWord!=None):
        cutTopRankedList.append(confusedWord)
    return cutTopRankedList


def find_confused_word(word):
    '''return the matching pair of a word from _CONFUSION_SET_ADDITIONS if a member of the pair passed in'''
    #deepcopy used as list of list returns reference to constant _CONFUSION_SET_ADDITIONS which would then be edited by following code
    confused_pair = copy.deepcopy(list(filter(lambda word_pairs: word in word_pairs, _CONFUSION_SET_ADDITIONS)))
    #if filter doesnt find a pair, the 'confused_pair' will be empty
    if len(confused_pair) != 0:
        confused_pair=confused_pair[0]
        confused_pair.remove(word)
        return confused_pair[0]
    else:
        return None


#function called from main candidates that normalises each probabiliy based on the highest in that category. 
#e.g greatest value of each candidate in P2 is normalised to 1 and all other candidate P2 values divided by the greatest.
def normalise(Pdict):
    '''normalises each P1, P2 and P3'''
    for i in range(0,3):
        maxP = 0
        for j in Pdict.keys():
            if Pdict[j][i] > maxP:
                maxP = Pdict[j][i]
        for x in (Pdict.keys()):
            #ensuring there is no division by 0
            if (maxP > 0):
                Pdict[x][i] = (Pdict[x][i])/(maxP)
    for a in Pdict.keys():
        Pdict[a][0]=Pdict[a][0]+Pdict[a][1]+Pdict[a][2]
        del Pdict[a][1]
        del Pdict[a][1]
    return Pdict



'''
Group: candidate subsets
this group of seventeen (four primary thirteen secondary) functions collects the known words generated by ite respective edits function
applys the probabilities to the set if it isnt empty
'''

def candidates_1_insert(word):
    word_copy = word
    candidates1_ins = (known(edits_1_insert(word_copy)))
    if (len(candidates1_ins) != 0):
        return apply_probabilities_1(word, candidates1_ins, "i")
    else:
        return {}


def candidates_1_delete(word):
    word_copy = word
    candidates1_del = (known(edits_1_delete(word_copy)))
    if (len(candidates1_del) != 0):
        return apply_probabilities_1(word, candidates1_del, "d")
    else:
        return {}


def candidates_1_substitute(word):
    word_copy = word
    candidates1_sub = (known(edits_1_substitute(word_copy)))
    if (len(candidates1_sub) != 0):
        return apply_probabilities_1(word, candidates1_sub, "s")
    else:
        return {}


def candidates_1_transpose(word):
    word_copy = word
    candidates1_trans = (known(edits_1_transpose(word_copy)))
    if (len(candidates1_trans) != 0):
        return apply_probabilities_1(word, candidates1_trans, "t")
    else:
        return {}


def candidates_2_ss(word):
    word_copy = word
    candidates_2_sswords = (known(edits_2_ss(word_copy)))
    if (len(candidates_2_sswords) != 0):
        return apply_probabilities_2('SS', candidates_2_sswords)
    else:
        return {}

def candidates_2_tt(word):
    word_copy = word
    candidates_2_ttwords = (known(edits_2_tt(word_copy)))
    if (len(candidates_2_ttwords) != 0):
        return apply_probabilities_2('TT', candidates_2_ttwords)
    else:
        return {}

def candidates_2_ii(word):
    word_copy = word
    candidates_2_iiwords = (known(edits_2_ii(word_copy)))
    if (len(candidates_2_iiwords) != 0):
        return apply_probabilities_2('II', candidates_2_iiwords)
    else:
        return {}

def candidates_2_dd(word):
    word_copy = word
    candidates_2_ddwords = (known(edits_2_dd(word_copy)))
    if (len(candidates_2_ddwords) != 0):
        return apply_probabilities_2('DD', candidates_2_ddwords)
    else:
        return {}

def candidates_2_st(word):
    word_copy = word
    candidates_2_stwords = (known(edits_2_st(word_copy)))
    if (len(candidates_2_stwords) != 0):
        return apply_probabilities_2('ST', candidates_2_stwords)
    else:
        return {}

#creates duplicates of candidates_2_si, left in to illustrate development desicion
'''
def candidates_2_si(word):
    word_copy = word
    candidates_2_siwords = (known(edits_2_si(word_copy)))
    if (len(candidates_2_siwords) != 0):
        return apply_probabilities_2('SI', candidates_2_siwords)
    else:
        return {}
'''

def candidates_2_is(word):
    word_copy = word
    candidates_2_iswords = (known(edits_2_is(word_copy)))
    if (len(candidates_2_iswords) != 0):
        return apply_probabilities_2('IS', candidates_2_iswords)
    else:
        return {}

def candidates_2_sd(word):
    word_copy = word
    candidates_2_sdwords = (known(edits_2_sd(word_copy)))
    if (len(candidates_2_sdwords) != 0):
        return apply_probabilities_2('SD', candidates_2_sdwords)
    else:
        return {}

def candidates_2_ds(word):
    word_copy = word
    candidates_2_dswords = (known(edits_2_ss(word_copy)))
    if (len(candidates_2_dswords) != 0):
        return apply_probabilities_2('DS', candidates_2_dswords)
    else:
        return {}

def candidates_2_ti(word):
    word_copy = word
    candidates_2_tiwords = (known(edits_2_ti(word_copy)))
    if (len(candidates_2_tiwords) != 0):
        return apply_probabilities_2('TI', candidates_2_tiwords)
    else:
        return {}

def candidates_2_it(word):
    word_copy = word
    candidates_2_itwords = (known(edits_2_it(word_copy)))
    if (len(candidates_2_itwords) != 0):
        return apply_probabilities_2('IT', candidates_2_itwords)
    else:
        return {}

def candidates_2_td(word):
    word_copy = word
    candidates_2_tdwords = (known(edits_2_td(word_copy)))
    if (len(candidates_2_tdwords) != 0):
        return apply_probabilities_2('TD', candidates_2_tdwords)
    else:
        return {}

def candidates_2_dt(word):
    word_copy = word
    candidates_2_dtwords = (known(edits_2_dt(word_copy)))
    if (len(candidates_2_dtwords) != 0):
        return apply_probabilities_2('DT', candidates_2_dtwords)
    else:
        return {}

#creates duplicates of candidates_2_di, left in to illustrate development desicion
'''
def candidates_2_id(word):
    wordCopy = word
    candidates_2_idwords = (known(edits_2_id(wordCopy)))
    if (len(candidates_2_idwords) != 0):
        return apply_probabilities_2('ID', candidates_2_idwords)
    else:
        return {}
'''
def candidates_2_di(word):
    wordCopy = word
    candidates_2_diwords = (known(edits_2_di(wordCopy)))
    if (len(candidates_2_diwords) != 0):
        return apply_probabilities_2('DI', candidates_2_diwords)
    else:
        return {}


#functions takes in strings and returns a list of those that are found in the dictionary
def known(words): 
    known=[]
    for w in words:
        if w in WORDS:
            known.append(w)
    return known


'''
Group: "apply Probablities" - two functions
returns {"word":[P1,P2,P3],....} where 'word' is the current candidate word that has been passed in and the P values correspond to;
1. a constant probability assigned based on type. 2. the probability if that word appearing in the corpus. 3. the confusion matrix probability based on church and gales research.
NOTICE: P3 only applys to words 1 edit away from the input. other words are assigned 0.0 for P3
The following group of two functions, made of one primary and one secondary, 
are called by their respective candidates functions and populate the [P1,P2,P3] value of each word in their respective candidates function. 
This must be done in the two seperate functions as a different constant probability are applied to each type of error in postion P1, a table is used to reduce redundant code.
'''
#probabilities stores the constant probabilities applied to 1-edits as P1
CONSTANT_PROBABILITIES_1 = {'i':0.15,'d':0.45,'s':0.3,'t':0.3}
def apply_probabilities_1(word, candidates, edit1Type):
    '''Apply P1, P2 and P3 to 1-edits'''
    #for this case, insert is least likely type, edits1 gives > p than edits2
    words_1_Prob = {}
    for w in candidates:
        words_1_Prob[w] = [0,0,0]
        #applys only when spellchecking english words
        if ENGLISH ==True:
            words_1_Prob[w][0] = CONSTANT_PROBABILITIES_1[edit1Type]
        words_1_Prob[w][1] = frequency_probability(w)
        #call relevant confusion matrix function to edit type
        if ENGLISH ==True:
            if edit1Type == 'i':
                words_1_Prob[w][2] = confusion_matrix_insert(word,w)
            elif edit1Type == 'd':
                words_1_Prob[w][2] = confusion_matrix_delete(word,w)
            elif edit1Type == 's':
                words_1_Prob[w][2] = confusion_matrix_substitute(word,w)
            elif edit1Type == 't':
                words_1_Prob[w][2] = confusion_matrix_transpose(word,w)
    return words_1_Prob


#probabilities case of 2 edits
CONSTANT_PROBABILITIES_2 = {'TT':0.15,'SS':0.15,'DD':0.225,'II':0.075,'SD':0.1875,'DS':0.1875,'SI':0.1125,
                   'IS':0.1125,'DT':0.375,'TD':0.1875,'TI':0.1125,'IT':0.1125,'ST':0.15,'DI':0.15, 'ID':0.15}
def apply_probabilities_2(editType, candidates):
    '''apply P1 and P2 to 2-edits'''
    words_2_Prob = {}
    for w in candidates:
        words_2_Prob[w] = [0.0,0.0,0.0]
        #only apply P1 when spellchecking english words
        if ENGLISH == True:
            words_2_Prob[w][0] = CONSTANT_PROBABILITIES_2[editType]
        words_2_Prob[w][1] = frequency_probability(w)
    return words_2_Prob



#gets the probability of each word in the corpus. P2 for assignment of probabilities to words.
WORD_FREQUENCE_TOTAL = WORDS["WordFrequencyTotal"][WORD_FREQ]
def frequency_probability(word): 
    return WORDS[word][WORD_FREQ] / WORD_FREQUENCE_TOTAL



'''
This section processes the Confusion matricies used by Church and Gale (APPENDIX E in report)
numpy (np.array_split) is used to convert the strings into 2D arrays
'''
alphaDict = {'a':0,'b':1,'c':2,'d':3,'e':4,'f':5,'g':6,'h':7,'i':8,'j':9,'k':10,'l':11,'m':12,'n':13,'o':14,'p':15,'q':16,'r':17,'s':18,'t':19,'u':20,'v':21,'w':22,'x':23,'y':24,'z':25}

ADStr = " 0 7 58 21 3 5 18 8 61 0 4 43 5 53 0 9 0 98 28 53 62 1 0 0 2 0 2 2 1 0 22 0 0 0 183 0 0 26 0 0 2 0 0 6 17 0 6 1 0 0 0 0 37 0 70 0 63 0 0 24 320 0 9 17 0 0 33 0 0 46 6 54 17 0 0 0 1 0 12 0 7 25 45 0 10 0 62 1 1 8 4 3 3 0 0 11 1 0 3 2 0 0 6 0 80 1 50 74 89 3 1 1 6 0 0 32 9 76 19 9 1 237 223 34 8 2 1 7 1 0 4 0 0 0 13 46 0 0 79 0 0 12 0 0 4 0 0 11 0 8 1 0 0 0 1 0 25 0 0 2 83 1 37 25 39 0 0 3 0 29 4 0 0 52 7 1 22 0 0 0 1 0 15 12 1 3 20 0 0 25 24 0 0 7 1 9 22 0 0 15 1 26 0 0 1 0 1 0 26 1 60 26 23 1 9 0 1 0 0 38 14 82 41 7 0 16 71 64 1 1 0 0 1 7 0 0 0 0 1 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 1 0 0 0 0 0 4 0 0 1 15 1 8 1 5 0 1 3 0 17 0 0 0 1 5 0 0 0 1 0 0 0 24 0 1 6 48 0 0 0 217 0 0 211 2 0 29 0 0 2 12 7 3 2 0 0 11 0 15 10 0 0 33 0 0 1 42 0 0 0 180 7 7 31 0 0 9 0 4 0 0 0 0 0 21 0 42 71 68 1 160 0 191 0 0 0 17 144 21 0 0 0 127 87 43 1 1 0 2 0 11 4 3 6 8 0 5 0 4 1 0 13 9 70 26 20 0 98 20 13 47 2 5 0 1 0 25 0 0 0 22 0 0 12 15 0 0 28 1 0 30 93 0 58 1 18 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 18 0 0 0 0 0 63 4 12 19 188 0 11 5 132 0 3 33 7 157 21 2 0 277 103 68 0 10 1 0 27 0 16 0 27 0 74 1 0 18 231 0 0 2 1 0 30 30 0 4 265 124 21 0 0 0 1 0 24 1 2 0 76 1 7 49 427 0 0 31 3 3 11 1 0 203 5 137 14 0 4 0 2 0 26 6 9 10 15 0 1 0 28 0 0 39 2 111 1 0 0 129 31 66 0 0 0 0 1 0 9 0 0 0 58 0 0 0 31 0 0 0 0 0 2 0 0 1 0 0 0 0 0 0 1 0 40 0 0 1 11 1 0 11 15 0 0 1 0 2 2 0 0 2 24 0 0 0 0 0 0 0 1 0 17 0 3 0 0 1 0 0 0 0 0 0 0 6 0 0 0 5 0 0 0 0 1 0 2 1 34 0 2 0 1 0 1 0 0 1 2 1 1 1 0 0 17 1 0 0 1 0 0 0 1 0 0 0 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 20 14 41 31 20 20 7 6 20 3 6 22 16 5 5 17 0 28 26 6 2 1 24 0 0 2"
AD = ADStr.split( )
AD = np.array_split(AD, 27)

ASStr = " 0 0 7 1 342 0 0 2 118 0 1 0 0 3 76 0 0 1 35 9 9 0 1 0 5 0 0 0 9 9 2 2 3 1 0 0 0 5 11 5 0 10 0 0 2 1 0 0 8 0 0 0 6 5 0 16 0 9 5 0 0 0 1 0 7 9 1 10 2 5 39 40 1 3 7 1 1 0 1 10 13 0 12 0 5 5 0 0 2 3 7 3 0 1 0 43 30 22 0 0 4 0 2 0 388 0 3 11 0 2 2 0 89 0 0 3 0 5 93 0 0 14 12 6 15 0 1 0 18 0 0 15 0 3 1 0 5 2 0 0 0 3 4 1 0 0 0 6 4 12 0 0 2 0 0 0 4 1 11 11 9 2 0 0 0 1 1 3 0 0 2 1 3 5 13 21 0 0 1 0 3 0 1 8 0 3 0 0 0 0 0 0 2 0 12 14 2 3 0 3 1 11 0 0 2 0 0 0 103 0 0 0 146 0 1 0 0 0 0 6 0 0 49 0 0 0 2 1 47 0 2 1 15 0 0 1 1 9 0 0 1 0 0 0 0 2 1 0 0 0 0 0 5 0 0 0 0 0 0 0 1 2 8 4 1 1 2 5 0 0 0 0 5 0 2 0 0 0 6 0 0 0 4 0 0 3 2 10 1 4 0 4 5 6 13 0 1 0 0 14 2 5 0 11 10 2 0 0 0 0 0 0 1 3 7 8 0 2 0 6 0 0 4 4 0 180 0 6 0 0 9 15 13 3 2 2 3 0 2 7 6 5 3 0 1 19 1 0 4 35 78 0 0 7 0 28 5 7 0 0 1 2 0 2 91 1 1 3 116 0 0 0 25 0 2 0 0 0 0 14 0 2 4 14 39 0 0 0 18 0 0 11 1 2 0 6 5 0 2 9 0 2 7 6 15 0 0 1 3 6 0 4 1 0 0 0 0 0 1 0 0 0 27 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 14 0 30 12 2 2 8 2 0 5 8 4 20 1 14 0 0 12 22 4 0 0 1 0 0 11 8 27 33 35 4 0 1 0 1 0 27 0 6 1 7 0 14 0 15 0 0 5 3 20 1 3 4 9 42 7 5 19 5 0 1 0 14 9 5 5 6 0 11 37 0 0 2 19 0 7 6 20 0 0 0 44 0 0 0 64 0 0 0 0 2 43 0 0 4 0 0 0 0 2 0 8 0 0 0 7 0 0 3 0 0 0 0 0 1 0 0 1 0 0 0 8 3 0 0 0 0 0 0 2 2 1 0 1 0 0 2 0 0 1 0 0 0 0 7 0 6 3 3 1 0 0 0 0 0 0 0 0 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0 9 0 0 0 0 0 0 0 0 0 2 0 15 0 1 7 15 0 0 0 2 0 6 1 0 7 36 8 5 0 0 1 0 0 0 0 0 7 0 0 0 0 0 0 0 7 5 0 0 0 0 2 21 3 0 0 0 0 3 0"
AS = ASStr.split( )
AS = np.array_split(AS, 26)

ATStr = "0 0 2 1 1 0 0 0 19 0 1 14 4 25 10 3 0 27 3 5 31 0 0 0 0 0 0 0 0 0 2 0 0 0 0 0 0 1 1 0 2 0 0 0 2 0 0 0 0 0 0 0 0 0 0 0 1 0 0 1 85 0 0 15 0 0 13 0 0 0 3 0 7 0 0 0 0 0 0 0 0 0 0 0 0 0 7 0 0 0 0 0 0 0 0 1 0 0 2 0 0 0 0 0 1 0 4 5 0 0 0 0 60 0 0 21 6 16 11 2 0 29 5 0 85 0 0 0 2 0 0 0 0 0 0 0 0 0 12 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 4 0 0 0 2 0 0 0 0 0 0 1 0 15 0 0 0 3 0 0 3 0 0 0 0 0 12 0 0 0 15 0 0 0 0 0 0 0 0 0 0 0 0 0 0 10 0 0 0 0 0 0 15 8 31 3 66 1 3 0 0 0 0 9 0 5 11 0 1 13 42 35 0 6 0 0 0 3 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 11 0 0 12 20 0 1 0 4 0 0 0 0 0 1 3 0 0 1 1 3 9 0 0 7 0 9 0 0 0 20 0 0 0 0 0 0 0 0 2 0 0 0 0 0 0 4 0 0 0 0 0 15 0 6 2 12 0 8 0 1 0 0 0 3 0 0 0 0 0 6 4 0 0 0 0 0 0 5 0 2 0 4 0 0 0 5 0 0 1 0 5 0 1 0 11 1 1 0 0 7 1 0 0 17 0 0 0 4 0 0 1 0 0 0 0 0 0 1 0 0 5 3 6 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 12 0 0 0 24 0 3 0 14 0 2 2 0 7 30 1 0 0 0 2 10 0 0 0 2 0 4 0 0 0 9 0 0 5 15 0 0 5 2 0 1 22 0 0 0 1 3 0 0 0 16 0 4 0 3 0 4 0 0 21 49 0 0 4 0 0 3 0 0 5 0 0 11 0 2 0 0 0 22 0 5 1 1 0 2 0 2 0 0 2 1 0 20 2 0 11 11 2 0 0 0 0 0 0 0 0 0 0 1 0 0 0 4 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 4 0 0 0 0 1 1 1 0 0 0 0 0 0 0 0 0 8 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 1 2 0 0 0 1 0 0 0 0 3 0 0 0 2 0 1 10 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0"
AT = ATStr.split( )
AT = np.array_split(AT, 26)

AIStr = "15 1 14 7 10 0 1 1 33 1 4 31 2 39 12 4 3 28 134 7 28 0 1 1 4 1 3 11 0 0 7 0 1 0 50 0 0 15 0 1 1 0 0 5 16 0 0 3 0 0 0 0 19 0 54 1 13 0 0 18 50 0 3 1 1 1 7 1 0 7 25 7 8 4 0 1 0 0 18 0 3 17 14 2 0 0 9 0 0 6 1 9 13 0 0 6 119 0 0 0 0 0 5 0 39 2 8 76 147 2 0 1 4 0 3 4 6 27 5 1 0 83 417 6 4 1 10 2 8 0 1 0 0 0 2 27 1 0 12 0 0 10 0 0 0 0 0 5 23 0 1 0 0 0 1 0 8 0 0 0 5 1 5 12 8 0 0 2 0 1 1 0 1 5 69 2 3 0 1 0 0 0 4 1 0 1 24 0 10 18 17 2 0 1 0 1 4 0 0 16 24 22 1 0 5 0 3 0 10 3 13 13 25 0 1 1 69 2 1 17 11 33 27 1 0 9 30 29 11 0 0 1 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 2 4 0 1 9 0 0 1 1 0 1 1 0 0 2 1 0 0 95 0 1 0 0 0 4 0 3 1 0 1 38 0 0 0 79 0 2 128 1 0 7 0 0 0 97 7 3 1 0 0 2 0 11 1 1 0 17 0 0 1 6 0 1 0 102 44 7 2 0 0 47 1 2 0 1 0 0 0 15 5 7 13 52 4 17 0 34 0 1 1 26 99 12 0 0 2 156 53 1 1 0 0 1 0 14 1 1 3 7 2 1 0 28 1 0 6 3 13 64 30 0 16 59 4 19 1 0 0 1 1 23 0 1 1 10 0 0 20 3 0 0 2 0 0 26 70 0 29 52 9 1 1 1 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 15 2 1 0 89 1 1 2 64 0 0 5 9 7 10 0 0 132 273 29 7 0 1 0 10 0 13 1 7 20 41 0 1 50 101 0 2 2 10 7 3 1 0 1 205 49 7 0 1 0 7 0 39 0 0 3 65 1 10 24 59 1 0 6 3 1 23 1 0 54 264 183 11 0 5 0 6 0 15 0 3 0 9 0 0 1 24 1 1 3 3 9 1 3 0 49 19 27 26 0 0 2 3 0 0 2 0 0 36 0 0 0 10 0 0 1 0 1 0 1 0 0 0 0 1 5 1 0 0 0 0 0 0 1 10 0 0 1 1 0 1 1 0 2 0 0 1 1 8 0 2 0 4 0 0 0 0 0 18 0 1 0 0 6 1 0 0 0 1 0 3 0 0 0 2 0 0 0 0 1 0 0 5 1 2 0 3 0 0 0 2 0 0 1 1 6 0 0 0 1 33 1 13 0 1 0 2 0 2 0 0 0 5 1 0 0 6 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 4 46 8 9 8 26 11 14 3 5 1 17 5 6 2 2 10 0 6 23 2 11 1 2 1 1 2"
AI = AIStr.split( )
AI = np.array_split(AI, 27)


TOTAL_MATRIX_SUM = 5716
'''
The following four functions use the confusion matricies and are called by the apply probablities functions and dictate the probablity value P3 for words 
generated by the primary edits functions
'''

def confusion_matrix_insert(word,candidate):
    '''get P3 value for 1 insert edits'''
    for i in range(len(word)):
        #check for different char in original word and this candidate
        if (word[i]!=candidate[i] and i>=1):
            j= alphaDict.get(word[i-1])
            k= alphaDict.get(word[i])
            freq = AI[j][k]
            probabilityI = int(freq)/TOTAL_MATRIX_SUM 
            return probabilityI

def confusion_matrix_delete(WP,W):
    '''get P3 value for 1 delete edits'''
    for l in range(len(W)):
        #check for different char in original word and this candidate
        if (WP[l]!=W[l] and l>=1):
            i = alphaDict.get(W[l-1])
            j = alphaDict.get(W[l])
            freq = AD[i][j]
            probabilityD = int(freq)/TOTAL_MATRIX_SUM 
            return probabilityD

def confusion_matrix_substitute(WP,W):
    '''get P3 value for 1 substitute edits'''
    for l in range(len(W)):
         #check for different char in original word and this candidate
        if WP[l]!=W[l]:
            i= alphaDict.get(WP[l])
            j= alphaDict.get(W[l])
            freq = AS[i][j]
            probabilityS = int(freq)/TOTAL_MATRIX_SUM 
            return probabilityS

def confusion_matrix_transpose(WP,W):
    '''get P3 value for 1 transpose edits'''
    for l in range(len(WP)):
         #check for different char in original word and this candidate
        if (WP[l]!=W[l] and l>=1 ):
            i= alphaDict.get(WP[l])
            j= alphaDict.get(W[l])
            freq = AT[i][j]
            probabilityT = int(freq)/TOTAL_MATRIX_SUM 
            return probabilityT



'''
Group: Primary Edits
This group of 4 functions takes in a misspelt word and returns an array of all strings with one of the 4 basic edits applied
these are; deletes, transpositions, substitutions, insertions.
each corresponds to and is called by its primary candidate subsets function.
Uses sets to eliminate duplicates (makes verifying in 'known' more efficient)
doesnt edit first and last character, unless a transposition
'''

LETTERS = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']

#'lru_cache' used to cache results from these functions. as they are called by functions in the secondary edit group
@lru_cache
def edits_1_insert(word):
    split_words = set()
    for insert_letter in LETTERS:
        for pos in range(1, len(word)):
            split_words.add(word[:pos]+insert_letter+word[pos:])
    return split_words

@lru_cache
def edits_1_delete(word):
    split_words=set()
    for i in range(1, len(word)-1):
        split_words.add(word[:i]+word[i+1:])
    return split_words

@lru_cache
def edits_1_substitute(word):
    split_words = set()
    for i in LETTERS:
        for j in range(1, len(word)-1):
            split_words.add(word[:j]+i+word[j+1:])
    return split_words

@lru_cache
def edits_1_transpose(word):
    split_words = set()
    for i in range(1, len(word)-1):
        split_words.add(word[:i]+word[i+1]+word[i]+word[i+2:])
    return split_words


'''the following 3 functions are used by the candidates_grammar_check function
seperate from the other primary edits functions as they edit the end of the 'word' parameter
the following fucntions don't edit the start of the 'word' parameter'''
@lru_cache
def edits_1_grammar_insert(word):
    split_words = set()
    for insert_letter in LETTERS:
        for pos in range(1, len(word)+1):
            split_words.add(word[:pos]+insert_letter+word[pos:])
    return split_words

@lru_cache
def edits_1_grammar_delete(word):
    split_words=set()
    for i in range(1, len(word)-1):
        split_words.add(word[:i]+word[i+1:])
    return split_words

@lru_cache
def edits_1_grammar_substitute(word):
    split_words = set()
    for i in LETTERS:
        for j in range(1, len(word)-1):
            split_words.add(word[:j]+i+word[j+1:])
    return split_words   

'''
Group: Secondary Edits
This group of 13 functions takes in a misspelt word and returns an array of all strings with two of the 4 basic edits applied
these are; II, DD, TT, SS, IT, TI, SI, DT, TD, SD, DS, ST, DI
where I= insertion, D= deletion , S= substitution and T= transposition.
NOTICE: ST and TS are associative, as are SI ans IS and DI and ID, these generate the same strings from the same word as one another,
so only one of them must be represented in a function

Each of these corresponds to and is called by its own candidate subsets function.
Uses sets to eliminate duplicates (makes verifying in 'known' more efficient)
'''

def edits_2_ss(word):
    edits_2_ss =set()
    for i1 in edits_1_substitute(word):
        for i2 in edits_1_substitute(i1):
            edits_2_ss.add(i2)
    return edits_2_ss

def edits_2_tt(word):
    edits_2_tt =set()
    for i1 in edits_1_transpose(word):
        for i2 in edits_1_transpose(i1):
            edits_2_tt.add(i2) 
    return edits_2_tt

def edits_2_ii(word):
    edits_2_ii =set()
    for i1 in edits_1_insert(word):
        for i2 in edits_1_insert(i1):
            edits_2_ii.add(i2) 
    return edits_2_ii

def edits_2_dd(word):
    edits_2_dd =set()
    if (len(word) < 6):
        #skip for words with 5 letters or less as deleting 2 char does not leave much that we want
        return edits_2_dd
    for i1 in edits_1_delete(word):
        for i2 in edits_1_delete(i1):
            edits_2_dd.add(i2)
    return edits_2_dd

def edits_2_st(word):
    edits_2_st =set()
    for i1 in (edits_1_substitute(word)):
        for i2 in edits_1_transpose(i1):
            edits_2_st.add(i2)
    return edits_2_st

#creates duplicate strings to edits_2_is
'''
def edits_2_si(word):
    edits_2_si =set()
    for i1 in (edits_1_substitute(word)):
        for i2 in edits_1_insert(i1):
            edits_2_si.add(i2)
    return edits_2_si
'''
def edits_2_is(word):
    edits_2_is =set()
    for i1 in (edits_1_insert(word)):
        for i2 in edits_1_substitute(i1):
            edits_2_is.add(i2)
    return edits_2_is

def edits_2_sd(word):   
    edits_2_sd =set()
    for i1 in (edits_1_substitute(word)):
        for i2 in edits_1_delete(i1):
            edits_2_sd.add(i2)
    return edits_2_sd

def edits_2_ds(word):
    edits_2_ds =set()
    for i1 in (edits_1_delete(word)):
        for i2 in edits_1_substitute(i1):
            edits_2_ds.add(i2)
    return edits_2_ds
      
def edits_2_ti(word):
    edits_2_ti =set()
    for i1 in (edits_1_transpose(word)):
        for i2 in edits_1_insert(i1):
            edits_2_ti.add(i2)
    return edits_2_ti

def edits_2_it(word):
    edits_2_it =set()
    for i1 in (edits_1_insert(word)):
        for i2 in edits_1_transpose(i1):
            edits_2_it.add(i2)
    return edits_2_it

def edits_2_td(word):
    edits_2_td =set()
    for i1 in (edits_1_transpose(word)):
        for i2 in edits_1_delete(i1):
            edits_2_td.add(i2)
    return edits_2_td

def edits_2_dt(word):
    edits_2_dt =set()
    for i1 in (edits_1_delete(word)):
        for i2 in edits_1_transpose(i1):
            edits_2_dt.add(i2)
    return edits_2_dt

def edits_2_di(word):
    edits_2_di = set()
    for i1 in (edits_1_delete(word)):
        for i2 in edits_1_insert(i1):
            edits_2_di.add(i2)
    return edits_2_di

#creates duplicate strings to edits_2_di
'''
def edits_2_id(word):
    edits_2_id = set()
    for i1 in (edits_1_insert(word)):
        for i2 in edits_1_delete(i1):
            edits_2_id.add(i2)
    return edits_2_id
'''


