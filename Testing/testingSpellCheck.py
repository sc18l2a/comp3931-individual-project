    
#testing the basic spellchecker 
from checkSpell import candidates
import json

with open('Dict1gram.json', 'r') as filehandle:
    WORDS = json.load(filehandle)



TEST_1 = 1
TEST_2 = 2
TEST_3 = 3
TEST_TYPE = TEST_3

file = open("spellTestSet.txt")
lines = file.readlines()
file.close()
#print(lines)

listLen = len(lines)
print(listLen)
num = 0
correctCount = 0
failCount = 0
testedCount =0
RealWordErrors=0
skipped=0
while (num < listLen-1):
    while lines[num][0] != '$':
        num += 1

    print("command word found : "+ lines[num])

    correctSpell = lines[num][1:].strip().lower()
    if '_' not in correctSpell:
        print("looking for "+correctSpell)
        num += 1
        while num!= listLen and lines[num][0] != '$':
            incorrectSpell = lines[num].split(' ')[0].strip().lower()
            if incorrectSpell not in WORDS:
                if '_' not in incorrectSpell:
                    print("testing " + correctSpell + "in candidates for "+ incorrectSpell)
                    candidates1 = (candidates(incorrectSpell))
                    candidatesList = []
                    if len(candidates1) > 0:
                        if TEST_TYPE == TEST_1:
                            candidatesList = candidates1
                        elif TEST_TYPE == TEST_2:
                            candidatesList = candidates1[0:3]
                        elif TEST_TYPE == TEST_3:
                            candidatesList = candidates1[0]
                        else:
                            print("Test type invalid, choose 1,2,3")
                            exit()
                        
                        if correctSpell in candidatesList:
                            print(candidates1)
                            print(correctSpell)
                            print(incorrectSpell)
                            correctCount += 1
                            testedCount += 1
                        else:
                            print(candidates1)
                            failCount += 1
                            testedCount += 1
            else:
                RealWordErrors+=1
            num += 1
    else:
        num+=1

print("FAILURES: "+ str(failCount))
print("PASSED: " + str(correctCount))
print("REAL WORD ERRORS: "+ str(RealWordErrors))

