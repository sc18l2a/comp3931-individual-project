
import unittest

from nltk.util import ngrams
from  checkSpell import *
from  checkGrammar import *


class testEdits(unittest.TestCase):
    def test_edits_1_delete(self):
        self.assertEqual(edits_1_delete("abc"),{'ac'})

    def test_edits_1_substitute(self):
        self.assertEqual(edits_1_substitute("abc"),{'ahc', 'abc', 'auc', 'aic', 'amc', 'ajc', 'aoc', 'agc', 'afc', 'alc', 'asc', 'awc', 'azc', 'apc', 'acc', 'arc', 'adc', 'axc', 'aac', 'aec', 'avc', 'aqc', 'ayc', 'anc', 'atc', 'akc'})

    def test_edits_1_insert(self):
        self.assertEqual(edits_1_insert("abc"),{'abpc', 'abuc', 'abwc', 'abgc', 'axbc', 'aibc', 'absc', 'abtc', 'abhc', 'anbc', 'afbc', 'agbc', 'abac', 'abec', 'aabc', 'abzc', 'abmc', 'aqbc', 'abjc', 'abqc', 'avbc', 'abdc', 'ablc', 'abrc', 'abfc', 'acbc', 'albc', 'apbc', 'aebc', 'abyc', 'ajbc', 'abvc', 'abxc', 'abbc', 'aobc', 'akbc', 'ambc', 'ahbc', 'atbc', 'abic', 'awbc', 'aubc', 'aybc', 'aboc', 'abcc', 'abkc', 'abnc', 'asbc', 'adbc', 'azbc', 'arbc'})

    def test_edits_1_transpose(self):
        self.assertEqual(edits_1_transpose("abc"),{'acb'})

    def test_edits_2_dd(self):
        self.assertEqual(edits_2_dd("abc"),set())

    def test_edits_2_ss(self):
        self.assertEqual(edits_2_ss("abc"),{'atc', 'aoc', 'aic', 'axc', 'akc', 'abc', 'asc', 'apc', 'awc', 'ayc', 'auc', 'alc', 'arc', 'acc', 'afc', 'amc', 'ajc', 'azc', 'avc', 'aac', 'agc', 'anc', 'aec', 'aqc', 'adc', 'ahc'})

    def test_edits_2_ii(self):
        self.assertEqual(edits_2_ii("abc"),{'abprc', 'agfbc', 'aecbc', 'aibmc', 'ahbvc', 'ailbc', 'aubpc', 'aubsc', 'abujc', 'aebyc', 'ajbsc', 'abrmc', 'ayxbc', 'abgoc', 'ablrc', 'abosc', 'aybdc', 'awpbc', 'abozc', 'adbvc', 'abasc', 'abvgc', 'aftbc', 'apbkc', 'abydc', 'afbpc', 'aiwbc', 'abdnc', 'aqpbc', 'adbrc', 'abvkc', 'anqbc', 'avbgc', 'afnbc', 'abnjc', 'abjqc', 'adsbc', 'algbc', 'aybsc', 'aghbc', 'abhtc', 'abuxc', 'agbwc', 'abmmc', 'absuc', 'alpbc', 'ablsc', 'abhdc', 'asbhc', 'alcbc', 'abptc', 'abtfc', 'ayfbc', 'atbvc', 'aebbc', 'abfhc', 'awbwc', 'alsbc', 'anbnc', 'alkbc', 'abbec', 'awbkc', 'aulbc', 'aberc', 'abxfc', 'avblc', 'abepc', 'abhwc', 'akbyc', 'apubc', 'avbyc', 'atkbc', 'amgbc', 'abngc', 'abfoc', 'aksbc', 'apbyc', 'aoebc', 'ambrc', 'aakbc', 'amcbc', 'abtzc', 'abpqc', 'aidbc', 'aubcc', 'anbxc', 'abvdc', 'axbyc', 'abvxc', 'abaqc', 'abmzc', 'afbgc', 'abtsc', 'awbbc', 'abakc', 'abqnc', 'ajnbc', 'awbyc', 'akbxc', 'awkbc', 'adbuc', 'abucc', 'afhbc', 'abnkc', 'ablpc', 'auwbc', 'ablnc', 'absdc', 'aabdc', 'aqboc', 'aumbc', 'abapc', 'abtbc', 'aebdc', 'arbdc', 'agebc', 'abjgc', 'ahibc', 'aelbc', 'aybec', 'apzbc', 'asrbc', 'ankbc', 'ayzbc', 'aibhc', 'akxbc', 'aqbac', 'afboc', 'acmbc', 'asbjc', 'azlbc', 'akbvc', 'abjmc', 'abbbc', 'arbqc', 'asbpc', 'afsbc', 'acbwc', 'awhbc', 'apbqc', 'awqbc', 'abgzc', 'abcwc', 'ahboc', 'abmyc', 'abxnc', 'aibcc', 'ajfbc', 'amlbc', 'awbgc', 'aivbc', 'aebsc', 'abylc', 'abodc', 'aribc', 'acbec', 'ayvbc', 'afblc', 'abesc', 'abxac', 'ahgbc', 'asmbc', 'arubc', 'agabc', 'azgbc', 'abvqc', 'abkxc', 'adrbc', 'abznc', 'aqbcc', 'abzoc', 'abcuc', 'aldbc', 'abxcc', 'awbxc', 'axkbc', 'aobtc', 'avbhc', 'azbvc', 'arkbc', 'aqnbc', 'abnnc', 'abkpc', 'abyjc', 'ahybc', 'axybc', 'aybrc', 'abzrc', 'atpbc', 'abogc', 'arybc', 'abpic', 'ayabc', 'afbuc', 'abumc', 'aabvc', 'abejc', 'afbwc', 'abioc', 'aaobc', 'aicbc', 'ansbc', 'auvbc', 'avobc', 'adlbc', 'abbqc', 'abtgc', 'adboc', 'aybwc', 'avbxc', 'arrbc', 'anblc', 'aboxc', 'abbhc', 'axebc', 'acbac', 'acbbc', 'abvvc', 'aofbc', 'abdhc', 'adbgc', 'anbvc', 'aqbic', 'arbfc', 'ajabc', 'abafc', 'akvbc', 'ahkbc', 'abxec', 'awblc', 'acblc', 'apblc', 'avvbc', 'abzbc', 'abagc', 'anzbc', 'adgbc', 'abfgc', 'ambuc', 'awbsc', 'ayblc', 'acbfc', 'avmbc', 'axbvc', 'abdvc', 'abpvc', 'agblc', 'adjbc', 'abtlc', 'asfbc', 'abwic', 'alibc', 'azbxc', 'axcbc', 'awwbc', 'abajc', 'abmpc', 'angbc', 'azbic', 'azpbc', 'abhcc', 'abvzc', 'almbc', 'aqkbc', 'agpbc', 'abyzc', 'abmwc', 'abhhc', 'agmbc', 'abuqc', 'ambec', 'aobpc', 'axbxc', 'aonbc', 'awabc', 'abkcc', 'abvmc', 'abaxc', 'amrbc', 'ablwc', 'abdgc', 'albmc', 'akbcc', 'abeqc', 'abpxc', 'anbdc', 'adbzc', 'asdbc', 'abtnc', 'abncc', 'akbgc', 'abmec', 'asbbc', 'atvbc', 'aodbc', 'abmcc', 'abhqc', 'aavbc', 'assbc', 'adobc', 'asbic', 'atdbc', 'aubzc', 'abrec', 'abshc', 'abgmc', 'albkc', 'akwbc', 'abzjc', 'aobfc', 'albec', 'acbdc', 'acbsc', 'abusc', 'ambtc', 'ahbic', 'aiabc', 'absfc', 'abwbc', 'awbnc', 'abqpc', 'anpbc', 'abxzc', 'abdyc', 'aukbc', 'abpoc', 'aqwbc', 'abckc', 'arbtc', 'apibc', 'aejbc', 'abegc', 'awibc', 'abnlc', 'abxuc', 'apybc', 'aqsbc', 'aeblc', 'aiobc', 'awbrc', 'ajrbc', 'abfdc', 'afbbc', 'abrtc', 'avpbc', 'abybc', 'asbgc', 'acbyc', 'albtc', 'agbac', 'aabnc', 'anvbc', 'anbwc', 'aambc', 'akboc', 'aubfc', 'aboac', 'axnbc', 'ajbgc', 'aibpc', 'abyyc', 'ayjbc', 'abwec', 'abeuc', 'asebc', 'apbvc', 'abidc', 'asxbc', 'auobc', 'ajibc', 'absoc', 'abyvc', 'awcbc', 'abgtc', 'abrvc', 'aubdc', 'aaybc', 'adbcc', 'alblc', 'amebc', 'abruc', 'aybac', 'aubtc', 'aozbc', 'ajbcc', 'amqbc', 'agbxc', 'atbqc', 'apbzc', 'abmdc', 'auqbc', 'abanc', 'aybpc', 'aobgc', 'aobsc', 'anbcc', 'ambwc', 'abinc', 'abmgc', 'ahabc', 'aohbc', 'actbc', 'abkuc', 'abhac', 'abzyc', 'acbnc', 'arbgc', 'asybc', 'abifc', 'ahbkc', 'aebuc', 'akbqc', 'abdrc', 'apbxc', 'awebc', 'ayboc', 'axbic', 'azhbc', 'alubc', 'axbac', 'abvic', 'aoboc', 'awnbc', 'abacc', 'anabc', 'abtmc', 'aibfc', 'anbrc', 'agbmc', 'ahhbc', 'axpbc', 'aqubc', 'abxyc', 'ajbnc', 'abflc', 'abjdc', 'axmbc', 'aokbc', 'abtwc', 'abskc', 'apbmc', 'aboqc', 'abqhc', 'aebgc', 'ajhbc', 'akbrc', 'abiuc', 'axdbc', 'abqjc', 'afebc', 'acjbc', 'ajbuc', 'aybgc', 'axbdc', 'abfkc', 'abycc', 'ahfbc', 'abqmc', 'absgc', 'afobc', 'ainbc', 'ambqc', 'abuzc', 'abeec', 'aqbsc', 'advbc', 'aqzbc', 'abiic', 'arwbc', 'antbc', 'abcvc', 'anbyc', 'aibqc', 'aibwc', 'abrdc', 'adcbc', 'abzxc', 'azzbc', 'abpyc', 'abqyc', 'abbic', 'abcfc', 'aebfc', 'ahjbc', 'akbpc', 'abstc', 'abxoc', 'asbkc', 'afbec', 'aoybc', 'abzfc', 'aspbc', 'abrqc', 'azdbc', 'abkgc', 'abvlc', 'acnbc', 'abamc', 'abhvc', 'abovc', 'atmbc', 'acbuc', 'aqbvc', 'agcbc', 'abomc', 'abiwc', 'abuyc', 'abbuc', 'ajblc', 'axbbc', 'afjbc', 'ackbc', 'albrc', 'atbwc', 'arcbc', 'amhbc', 'abobc', 'afkbc', 'alzbc', 'ajkbc', 'andbc', 'aboic', 'aggbc', 'abbyc', 'axbsc', 'auybc', 'abrhc', 'abedc', 'ahlbc', 'adblc', 'adpbc', 'asbmc', 'abbdc', 'appbc', 'ajdbc', 'agbpc', 'aglbc', 'arboc', 'abfyc', 'aurbc', 'abipc', 'abmkc', 'abwxc', 'afbnc', 'abqvc', 'aeybc', 'aebrc', 'abmac', 'abgic', 'aclbc', 'aqbjc', 'aibzc', 'aznbc', 'ahbxc', 'abnpc', 'azbrc', 'aeboc', 'abowc', 'aabhc', 'abmrc', 'abfxc', 'apbsc', 'abctc', 'allbc', 'aedbc', 'aunbc', 'abqic', 'abrbc', 'alabc', 'ahtbc', 'acdbc', 'amkbc', 'anbec', 'abjcc', 'acfbc', 'albac', 'aabfc', 'abauc', 'abvyc', 'axbqc', 'abfcc', 'aqbpc', 'abjjc', 'abwkc', 'amsbc', 'abiac', 'afabc', 'ablkc', 'akkbc', 'abymc', 'artbc', 'abdtc', 'agybc', 'aobvc', 'aacbc', 'abmoc', 'abhfc', 'apvbc', 'agubc', 'alebc', 'ablic', 'ajebc', 'auhbc', 'abthc', 'arbhc', 'agibc', 'adbqc', 'axbkc', 'albuc', 'abkdc', 'axbpc', 'ajvbc', 'atcbc', 'abdec', 'awbtc', 'abbxc', 'abjuc', 'akobc', 'abwsc', 'apjbc', 'atbic', 'agbjc', 'abqfc', 'awbjc', 'adbmc', 'atbgc', 'aprbc', 'ahbnc', 'abojc', 'abmsc', 'aigbc', 'ascbc', 'abyoc', 'albfc', 'abwjc', 'abehc', 'aqqbc', 'ajobc', 'asbcc', 'abvnc', 'azbgc', 'albqc', 'ammbc', 'abgjc', 'astbc', 'ajqbc', 'absyc', 'agbuc', 'abmfc', 'abfrc', 'aubwc', 'anbjc', 'abftc', 'albpc', 'agbtc', 'akfbc', 'atbkc', 'audbc', 'abewc', 'abysc', 'aqbbc', 'arblc', 'argbc', 'aibec', 'abtxc', 'abnxc', 'abkoc', 'agnbc', 'abtdc', 'arbac', 'axvbc', 'ahbsc', 'aobkc', 'abygc', 'acbic', 'aeubc', 'apebc', 'awboc', 'abgsc', 'aqbkc', 'asubc', 'aqabc', 'auxbc', 'avbrc', 'albhc', 'azxbc', 'abpgc', 'agboc', 'abzdc', 'axbrc', 'abqqc', 'avbwc', 'aibyc', 'atbjc', 'adbpc', 'abzuc', 'apsbc', 'abvrc', 'abbac', 'abotc', 'ayybc', 'adbxc', 'abmic', 'azbyc', 'abkwc', 'asbwc', 'aqtbc', 'axbhc', 'azbac', 'aygbc', 'abyuc', 'agbnc', 'acsbc', 'axfbc', 'adybc', 'adbhc', 'agbfc', 'axgbc', 'aaboc', 'abcbc', 'albxc', 'abkfc', 'arbec', 'avbuc', 'acbpc', 'arebc', 'aabxc', 'avboc', 'abvac', 'ahdbc', 'akcbc', 'arzbc', 'abrnc', 'abzmc', 'ablbc', 'abxwc', 'albvc', 'anbgc', 'aahbc', 'atbtc', 'abryc', 'abntc', 'aebvc', 'abgrc', 'aqbzc', 'afbkc', 'arabc', 'abfec', 'aenbc', 'aoblc', 'abtoc', 'akbbc', 'alvbc', 'ajbpc', 'akbec', 'abpbc', 'abmhc', 'ayqbc', 'afbcc', 'anbbc', 'ablfc', 'aqbmc', 'abcrc', 'abpnc', 'abkzc', 'awbhc', 'abtec', 'abdlc', 'abzac', 'avbec', 'abzqc', 'abpsc', 'abzcc', 'afqbc', 'abqzc', 'arvbc', 'apkbc', 'azobc', 'abnec', 'abqrc', 'aihbc', 'aabrc', 'aujbc', 'awbzc', 'asbnc', 'abefc', 'abzec', 'anmbc', 'alobc', 'atubc', 'abxxc', 'abarc', 'adbkc', 'abilc', 'ahbfc', 'abcyc', 'ahmbc', 'acbmc', 'akgbc', 'asjbc', 'aubrc', 'ambcc', 'acbgc', 'agsbc', 'ajsbc', 'ahbzc', 'azbbc', 'abwgc', 'abjsc', 'aizbc', 'abypc', 'abhlc', 'axbgc', 'abwqc', 'anlbc', 'agbhc', 'absbc', 'akrbc', 'apfbc', 'abjzc', 'abadc', 'asqbc', 'aubvc', 'avbac', 'atbsc', 'abccc', 'aimbc', 'abzvc', 'abmvc', 'aebwc', 'aembc', 'absjc', 'abxjc', 'abljc', 'apbpc', 'aebtc', 'abgec', 'abufc', 'aiibc', 'aqblc', 'anjbc', 'ambsc', 'abjkc', 'aqfbc', 'anbic', 'aabwc', 'aafbc', 'abqsc', 'aybmc', 'auebc', 'apbfc', 'aezbc', 'abiqc', 'acgbc', 'abnwc', 'aobmc', 'aebcc', 'abtvc', 'awubc', 'afbtc', 'atbyc', 'acbkc', 'abhic', 'abxhc', 'aobnc', 'abvpc', 'abloc', 'ajbic', 'apbdc', 'axjbc', 'azboc', 'abdwc', 'abvoc', 'abvwc', 'agbdc', 'axblc', 'abmbc', 'abkrc', 'abibc', 'akbfc', 'awbpc', 'abpwc', 'abvec', 'asbqc', 'aabsc', 'anubc', 'aikbc', 'azsbc', 'abrac', 'arbnc', 'abcdc', 'abquc', 'abnsc', 'abetc', 'abeic', 'afgbc', 'avbnc', 'abwac', 'adxbc', 'acobc', 'alhbc', 'ambpc', 'abdfc', 'asbvc', 'aubac', 'abisc', 'axabc', 'abgbc', 'abwtc', 'abspc', 'abjlc', 'abfnc', 'avjbc', 'awlbc', 'alwbc', 'abfbc', 'apbcc', 'abnzc', 'agbzc', 'abqec', 'arbcc', 'akbhc', 'aboyc', 'awbuc', 'anbuc', 'abonc', 'abukc', 'aybbc', 'aobzc', 'ahnbc', 'ahxbc', 'ashbc', 'atbnc', 'avtbc', 'anbtc', 'acubc', 'aubbc', 'abwvc', 'abbwc', 'aorbc', 'aubxc', 'abxsc', 'abbfc', 'aymbc', 'aebac', 'aadbc', 'abhmc', 'avebc', 'arbrc', 'abjfc', 'abcic', 'afpbc', 'abzsc', 'akbsc', 'arbpc', 'avrbc', 'axbcc', 'afybc', 'aebjc', 'avxbc', 'abcxc', 'atwbc', 'apbuc', 'aqmbc', 'atbxc', 'abohc', 'ahobc', 'aoibc', 'aobyc', 'ardbc', 'aobuc', 'aibgc', 'abaec', 'abkvc', 'augbc', 'aibsc', 'akybc', 'abgqc', 'aplbc', 'abhpc', 'aztbc', 'abhzc', 'aanbc', 'ambac', 'ajbrc', 'abnic', 'amabc', 'abboc', 'afzbc', 'aslbc', 'alybc', 'ajbhc', 'abktc', 'aqbxc', 'abyic', 'abcmc', 'acpbc', 'aqcbc', 'abxtc', 'avbbc', 'acebc', 'abhbc', 'affbc', 'aphbc', 'abgwc', 'abwuc', 'acwbc', 'aebxc', 'afbmc', 'awbic', 'aoqbc', 'awobc', 'askbc', 'abjpc', 'aobxc', 'aobbc', 'aqobc', 'aybuc', 'arfbc', 'abuic', 'abqdc', 'aajbc', 'aebic', 'atbac', 'axubc', 'alrbc', 'apbnc', 'asvbc', 'abaac', 'abbpc', 'awzbc', 'arbsc', 'agdbc', 'aoobc', 'axobc', 'aiebc', 'awvbc', 'absrc', 'avbzc', 'aetbc', 'ambdc', 'abqcc', 'aqdbc', 'acrbc', 'abrzc', 'abllc', 'aubkc', 'abwpc', 'abgdc', 'asbyc', 'abpzc', 'axrbc', 'abazc', 'azqbc', 'abwdc', 'aiblc', 'abksc', 'apbwc', 'aibic', 'abtjc', 'ablac', 'abdzc', 'abayc', 'aqbgc', 'aabqc', 'ahbbc', 'abwlc', 'aoxbc', 'aipbc', 'albic', 'abvcc', 'abkmc', 'aiybc', 'ahsbc', 'avubc', 'abxrc', 'awybc', 'abcgc', 'ajbxc', 'abelc', 'abhkc', 'abuwc', 'aybvc', 'abluc', 'abggc', 'adhbc', 'aabtc', 'abzic', 'afbfc', 'axqbc', 'adbbc', 'azbjc', 'abouc', 'abtac', 'axbmc', 'aabac', 'arbmc', 'aobqc', 'aeibc', 'abduc', 'aabmc', 'abjwc', 'apbgc', 'acibc', 'abslc', 'aobjc', 'aqvbc', 'aibuc', 'aabzc', 'abwwc', 'ayobc', 'awbcc', 'aybnc', 'aybcc', 'abbrc', 'aywbc', 'aibxc', 'aytbc', 'abirc', 'ampbc', 'ahqbc', 'abnmc', 'abytc', 'asabc', 'abguc', 'avcbc', 'akjbc', 'asibc', 'abscc', 'abahc', 'awrbc', 'abuvc', 'adbyc', 'abaic', 'achbc', 'apbic', 'aitbc', 'aoubc', 'aisbc', 'avlbc', 'abulc', 'afbsc', 'asbfc', 'acboc', 'abswc', 'ahrbc', 'aaubc', 'aaabc', 'apbhc', 'ajbbc', 'ahbyc', 'adkbc', 'aubec', 'akibc', 'atabc', 'abtkc', 'ajtbc', 'adbfc', 'aqgbc', 'avbjc', 'akbzc', 'ajbfc', 'aehbc', 'abdic', 'abvjc', 'abgkc', 'azybc', 'ahbrc', 'abfuc', 'adzbc', 'ajgbc', 'ababc', 'abfqc', 'abdsc', 'avbcc', 'abqkc', 'aybhc', 'abhoc', 'abhjc', 'azwbc', 'adbac', 'asbec', 'aqbyc', 'abdxc', 'abnhc', 'aqebc', 'abqlc', 'annbc', 'arbic', 'ahcbc', 'abtic', 'abblc', 'arbjc', 'abdoc', 'aqrbc', 'apabc', 'abykc', 'adbsc', 'avfbc', 'abroc', 'abgxc', 'aublc', 'amtbc', 'azibc', 'anbqc', 'abgvc', 'ablqc', 'adbjc', 'aynbc', 'abwyc', 'amibc', 'abjec', 'abzgc', 'amobc', 'afcbc', 'afvbc', 'aybzc', 'azubc', 'abatc', 'abfjc', 'abcoc', 'anboc', 'arbvc', 'arbkc', 'abzhc', 'akzbc', 'abpkc', 'athbc', 'avgbc', 'aubnc', 'avdbc', 'agbbc', 'aesbc', 'aobcc', 'asbrc', 'abunc', 'ajwbc', 'abaoc', 'avbpc', 'anbsc', 'apmbc', 'apbec', 'ambzc', 'azfbc', 'akubc', 'abolc', 'akbdc', 'atbdc', 'abhgc', 'abbvc', 'aljbc', 'abdac', 'atbfc', 'agwbc', 'acbvc', 'aybxc', 'admbc', 'avbfc', 'albsc', 'abivc', 'abbzc', 'abfpc', 'ajybc', 'abcnc', 'anybc', 'azbzc', 'abcjc', 'abjic', 'amboc', 'ayibc', 'aowbc', 'ajbmc', 'abcsc', 'azbtc', 'afbqc', 'asbdc', 'aazbc', 'acbtc', 'avybc', 'albbc', 'ahvbc', 'awbfc', 'aybjc', 'akbtc', 'arxbc', 'aevbc', 'avbdc', 'agbic', 'abltc', 'aeebc', 'aswbc', 'abhec', 'afbac', 'afbdc', 'abrpc', 'aybfc', 'agbsc', 'anxbc', 'awmbc', 'azbsc', 'ajbec', 'auubc', 'ambvc', 'aycbc', 'abbjc', 'auabc', 'aabgc', 'avqbc', 'axboc', 'agvbc', 'anbhc', 'aydbc', 'ajbdc', 'abzpc', 'abtqc', 'ahubc', 'abxvc', 'adabc', 'amfbc', 'abgnc', 'abxic', 'abhsc', 'abglc', 'abdqc', 'abnoc', 'awbdc', 'abbkc', 'abemc', 'afibc', 'abjvc', 'asobc', 'ambhc', 'agbyc', 'abffc', 'afxbc', 'ahebc', 'abdmc', 'abzkc', 'acbjc', 'aubuc', 'adubc', 'abevc', 'aibvc', 'azbqc', 'abvsc', 'abric', 'abttc', 'aegbc', 'aysbc', 'aebqc', 'abwmc', 'abywc', 'axbec', 'abwzc', 'abzzc', 'avbmc', 'abwhc', 'axxbc', 'atbrc', 'abjhc', 'abtyc', 'aqbwc', 'axbuc', 'abkec', 'azbuc', 'asnbc', 'addbc', 'aarbc', 'abvhc', 'avkbc', 'abfsc', 'abkkc', 'aufbc', 'arjbc', 'avnbc', 'apdbc', 'axhbc', 'aibkc', 'abudc', 'aubyc', 'aszbc', 'abhnc', 'abpec', 'agbrc', 'abmxc', 'abfac', 'avbqc', 'abnvc', 'apbac', 'akabc', 'azbhc', 'aijbc', 'akbmc', 'aebec', 'aabpc', 'aombc', 'arnbc', 'attbc', 'ajjbc', 'avabc', 'akblc', 'abxgc', 'atlbc', 'asbuc', 'awdbc', 'asbac', 'aojbc', 'aibac', 'abihc', 'abenc', 'apbrc', 'amubc', 'aebpc', 'abnbc', 'azjbc', 'aobic', 'abjoc', 'ahwbc', 'aqlbc', 'akbwc', 'abvfc', 'acbrc', 'aexbc', 'abqac', 'aiubc', 'atbec', 'arbbc', 'awbvc', 'abwnc', 'abxmc', 'acabc', 'ahbqc', 'avbsc', 'aobec', 'abjtc', 'ablxc', 'anebc', 'abebc', 'abnuc', 'axwbc', 'aabec', 'aiboc', 'albjc', 'abhxc', 'abkyc', 'absqc', 'abwcc', 'adibc', 'ajbtc', 'atgbc', 'ambfc', 'abwoc', 'abpuc', 'azbfc', 'aewbc', 'anbzc', 'avbtc', 'aovbc', 'abyfc', 'aypbc', 'ahbdc', 'aebzc', 'azvbc', 'asboc', 'abekc', 'ambmc', 'abhyc', 'abvtc', 'abiyc', 'abrsc', 'abuoc', 'abrfc', 'abphc', 'ajboc', 'abync', 'ahbjc', 'aobac', 'atobc', 'aaebc', 'aobhc', 'abpcc', 'atboc', 'abdbc', 'azabc', 'abyxc', 'akbuc', 'aaxbc', 'aebkc', 'aucbc', 'amnbc', 'abimc', 'abezc', 'abmnc', 'armbc', 'ahbgc', 'abmuc', 'abgpc', 'aekbc', 'acxbc', 'albdc', 'abqbc', 'avbvc', 'aburc', 'aqbnc', 'atebc', 'agkbc', 'ahblc', 'absic', 'adwbc', 'abjxc', 'ayrbc', 'ajubc', 'apobc', 'ablvc', 'abplc', 'aknbc', 'anibc', 'abugc', 'agbec', 'abijc', 'arbxc', 'abkhc', 'ajlbc', 'ablyc', 'avwbc', 'abddc', 'abwfc', 'ajbac', 'abghc', 'abqoc', 'abuac', 'azbec', 'auibc', 'afwbc', 'abiec', 'adtbc', 'abooc', 'abavc', 'agbkc', 'avzbc', 'aybyc', 'ahbhc', 'abzwc', 'aykbc', 'abszc', 'aybkc', 'agbqc', 'aptbc', 'asbsc', 'aaibc', 'agzbc', 'ajbyc', 'arsbc', 'absac', 'apbbc', 'atbhc', 'aibbc', 'abbcc', 'alboc', 'ajbkc', 'aopbc', 'abmtc', 'abcqc', 'afbjc', 'aixbc', 'abexc', 'axlbc', 'abgac', 'apbtc', 'acbzc', 'acbxc', 'abfzc', 'axbnc', 'avbkc', 'azrbc', 'aqbhc', 'auboc', 'abigc', 'afbyc', 'aborc', 'abixc', 'ahbpc', 'aibrc', 'agobc', 'akbac', 'abofc', 'abcec', 'abclc', 'acqbc', 'abztc', 'abczc', 'abfwc', 'absnc', 'aotbc', 'atrbc', 'anbfc', 'adebc', 'abmlc', 'aczbc', 'abxdc', 'aybic', 'axzbc', 'abutc', 'ancbc', 'abyec', 'anbmc', 'abfic', 'abjbc', 'ajbzc', 'ausbc', 'afmbc', 'ahbec', 'akbic', 'ahbuc', 'aobdc', 'ahzbc', 'aepbc', 'aeqbc', 'aasbc', 'aabuc', 'anobc', 'abppc', 'absec', 'ajpbc', 'aebnc', 'acbhc', 'abtpc', 'abalc', 'arbyc', 'ajbvc', 'amblc', 'axibc', 'abeoc', 'ambic', 'atfbc', 'auzbc', 'abnqc', 'axbtc', 'atsbc', 'atbzc', 'aktbc', 'azbmc', 'abpdc', 'ajxbc', 'ablmc', 'apcbc', 'agxbc', 'adbnc', 'abupc', 'abpjc', 'abawc', 'abrrc', 'agqbc', 'adnbc', 'anhbc', 'aabkc', 'aqbqc', 'absmc', 'abqwc', 'abxbc', 'amjbc', 'azbdc', 'aebhc', 'azbwc', 'abrxc', 'aubic', 'abkac', 'awjbc', 'aosbc', 'apgbc', 'azbnc', 'arobc', 'abcac', 'azebc', 'adbec', 'abwrc', 'albyc', 'aabyc', 'albzc', 'ambxc', 'absvc', 'abbnc', 'abkjc', 'ajcbc', 'asblc', 'agtbc', 'ablgc', 'acvbc', 'atzbc', 'avibc', 'apxbc', 'awbqc', 'abjac', 'abgcc', 'albcc', 'amxbc', 'abikc', 'aerbc', 'abrwc', 'apnbc', 'altbc', 'akhbc', 'aagbc', 'abeyc', 'ablzc', 'abxpc', 'atnbc', 'ambjc', 'aybtc', 'asbtc', 'abgfc', 'anrbc', 'aubgc', 'abnyc', 'aablc', 'apboc', 'aibnc', 'arbwc', 'ajmbc', 'aqbfc', 'abxkc', 'akbjc', 'aboec', 'avhbc', 'aqbdc', 'aobrc', 'accbc', 'aabic', 'abklc', 'abdpc', 'amybc', 'axbwc', 'akebc', 'atbcc', 'abokc', 'atjbc', 'abdcc', 'abldc', 'abfvc', 'abyqc', 'abrlc', 'adqbc', 'aabbc', 'awgbc', 'acbqc', 'agbcc', 'abopc', 'aybqc', 'afbzc', 'atbuc', 'aubmc', 'absxc', 'aupbc', 'avbic', 'aibjc', 'abrcc', 'aapbc', 'akqbc', 'agjbc', 'aalbc', 'abrkc', 'afbhc', 'atblc', 'abnac', 'abocc', 'abmqc', 'aibtc', 'aqbtc', 'aqhbc', 'afdbc', 'aflbc', 'abndc', 'afbrc', 'aolbc', 'abubc', 'azbkc', 'azkbc', 'abpmc', 'aqxbc', 'afbic', 'abmjc', 'axsbc', 'ajzbc', 'aibdc', 'apwbc', 'azcbc', 'abuhc', 'ahbwc', 'aeabc', 'awbmc', 'aubjc', 'abecc', 'aogbc', 'akbkc', 'albgc', 'acybc', 'ajbqc', 'abjnc', 'abbgc', 'arbuc', 'aawbc', 'abeac', 'abxlc', 'abrgc', 'aifbc', 'ambnc', 'amdbc', 'abdkc', 'amwbc', 'abizc', 'abchc', 'aubhc', 'akdbc', 'abjyc', 'atbpc', 'agbvc', 'aqjbc', 'azmbc', 'abrjc', 'axbzc', 'abhrc', 'ambyc', 'atbmc', 'anwbc', 'afbxc', 'axbfc', 'aebmc', 'awxbc', 'adbtc', 'asgbc', 'abtrc', 'aabjc', 'abyac', 'afubc', 'abcpc', 'anbkc', 'abqtc', 'ahbac', 'albwc', 'abnrc', 'abicc', 'ahpbc', 'aatbc', 'abbtc', 'abtcc', 'axbjc', 'ayubc', 'atxbc', 'aubqc', 'arhbc', 'abnfc', 'ahbcc', 'abgyc', 'abdjc', 'anbac', 'arlbc', 'ajbwc', 'ajbjc', 'atqbc', 'aabcc', 'abpac', 'abbmc', 'aobwc', 'amzbc', 'abuec', 'aoabc', 'aqibc', 'aocbc', 'aqbec', 'abbsc', 'axtbc', 'awfbc', 'asbzc', 'abtuc', 'abkic', 'abpfc', 'ambkc', 'adbwc', 'awtbc', 'adbic', 'autbc', 'akbnc', 'atbbc', 'arqbc', 'abkbc', 'alqbc', 'afbvc', 'atibc', 'aylbc', 'awsbc', 'abzlc', 'aiqbc', 'awbac', 'aeobc', 'adfbc', 'ayebc', 'amvbc', 'ahbmc', 'azbpc', 'abjrc', 'azbcc', 'abuuc', 'abyhc', 'abqgc', 'ambbc', 'akmbc', 'adbdc', 'apqbc', 'ambgc', 'apbjc', 'abyrc', 'abvbc', 'abhuc', 'abknc', 'ahbtc', 'acbcc', 'abfmc', 'ayhbc', 'asbxc', 'abkqc', 'abitc', 'abssc', 'anbpc', 'albnc', 'airbc', 'alfbc', 'afrbc', 'agrbc', 'aqbuc', 'arpbc', 'avsbc', 'abqxc', 'ablcc', 'agbgc', 'akpbc', 'abxqc', 'azblc', 'aqbrc', 'abvuc', 'alxbc', 'aaqbc', 'alnbc', 'ablhc', 'anfbc', 'aklbc', 'atybc', 'awbec', 'arbzc', 'ablec', 'aqybc', 'aefbc'})

    def test_edits_2_tt(self):
        self.assertEqual(edits_2_tt("abc"),{'abc'})

    def test_edits_2_st(self):
        self.assertEqual(edits_2_st("abc"),{'acz', 'acr', 'acf', 'acq', 'acd', 'acb', 'acp', 'aca', 'aco', 'acy', 'acv', 'acl', 'acn', 'acg', 'ach', 'acu', 'acm', 'ace', 'act', 'acc', 'acx', 'aci', 'ack', 'acw', 'acs', 'acj'})

    def test_edits_2_ds(self):
        self.assertEqual(edits_2_ds("abc"),set())
    
    def test_edits_2_sd(self):
        self.assertEqual(edits_2_sd("abc"),{'ac'})
        
    def test_edits_2_dt(self):
        self.assertEqual(edits_2_dt("abc"),set())

    def test_edits_2_td(self):
        self.assertEqual(edits_2_td("abc"),{'ab'})

    def test_edits_2_is(self):
        self.assertEqual(edits_2_is("abc"),{'ayzc', 'azbc', 'apgc', 'aohc', 'amic', 'axac', 'arnc', 'awwc', 'aedc', 'azic', 'abac', 'askc', 'ajrc', 'ahhc', 'axuc', 'agvc', 'akyc', 'agpc', 'armc', 'arxc', 'atkc', 'adac', 'ashc', 'atbc', 'aphc', 'avcc', 'avpc', 'auec', 'azxc', 'aefc', 'amcc', 'abnc', 'ayxc', 'auxc', 'axcc', 'aabc', 'ansc', 'aync', 'algc', 'ajhc', 'amtc', 'afsc', 'anec', 'ayec', 'apjc', 'aeic', 'aazc', 'alkc', 'acvc', 'asvc', 'atsc', 'auac', 'auzc', 'alcc', 'awsc', 'acuc', 'aktc', 'aarc', 'axtc', 'aasc', 'aaoc', 'aesc', 'atmc', 'avkc', 'ascc', 'ajkc', 'afyc', 'apwc', 'aaxc', 'apdc', 'ajnc', 'ayuc', 'awuc', 'azec', 'azrc', 'agkc', 'afjc', 'asrc', 'axxc', 'aotc', 'awyc', 'afec', 'aadc', 'awxc', 'akqc', 'aayc', 'axwc', 'ahqc', 'auwc', 'apac', 'axsc', 'aawc', 'akvc', 'amuc', 'accc', 'aboc', 'axfc', 'akpc', 'acsc', 'aydc', 'axvc', 'akuc', 'anxc', 'aujc', 'ahvc', 'aovc', 'arzc', 'aibc', 'avwc', 'ahrc', 'ahdc', 'amsc', 'aclc', 'apec', 'apbc', 'avuc', 'arvc', 'afcc', 'aocc', 'aluc', 'aixc', 'aevc', 'aumc', 'acac', 'agzc', 'actc', 'avmc', 'adcc', 'adfc', 'alxc', 'ardc', 'aqrc', 'aooc', 'affc', 'awcc', 'abdc', 'amxc', 'aojc', 'aobc', 'azsc', 'anpc', 'amgc', 'avxc', 'ackc', 'anbc', 'aroc', 'aisc', 'aemc', 'atac', 'adyc', 'axoc', 'awqc', 'auqc', 'aitc', 'auyc', 'afoc', 'aomc', 'anac', 'acwc', 'apxc', 'amdc', 'akac', 'arjc', 'aiic', 'akic', 'aurc', 'aklc', 'atxc', 'akec', 'atzc', 'aapc', 'atcc', 'amqc', 'awlc', 'ajcc', 'aeyc', 'avsc', 'abuc', 'aakc', 'aqbc', 'aloc', 'ayjc', 'akjc', 'aoqc', 'atyc', 'adzc', 'abxc', 'amlc', 'aypc', 'axmc', 'abfc', 'awdc', 'avec', 'amoc', 'atuc', 'aacc', 'azoc', 'aflc', 'afbc', 'ajbc', 'aoac', 'awec', 'aamc', 'aiuc', 'alfc', 'almc', 'aunc', 'apkc', 'akmc', 'aoec', 'arwc', 'abbc', 'aipc', 'axqc', 'avic', 'aqzc', 'ajpc', 'ammc', 'awtc', 'abec', 'avtc', 'awrc', 'aavc', 'axpc', 'ajec', 'aukc', 'asqc', 'adgc', 'ayrc', 'aagc', 'apsc', 'ajac', 'arbc', 'arfc', 'athc', 'achc', 'apoc', 'abzc', 'aswc', 'afkc', 'anlc', 'aysc', 'ambc', 'aqlc', 'akoc', 'aimc', 'atfc', 'avqc', 'adpc', 'ayoc', 'alpc', 'aekc', 'akkc', 'asbc', 'aanc', 'adoc', 'aoic', 'aecc', 'afhc', 'afnc', 'aqwc', 'augc', 'atpc', 'annc', 'ablc', 'aqkc', 'atec', 'amzc', 'allc', 'aatc', 'ahjc', 'adhc', 'amhc', 'axbc', 'abkc', 'aeqc', 'awzc', 'aznc', 'avfc', 'axec', 'axkc', 'aufc', 'awhc', 'afmc', 'ahzc', 'ajsc', 'axdc', 'aqsc', 'aosc', 'aahc', 'aylc', 'akcc', 'apzc', 'azhc', 'afdc', 'axgc', 'aubc', 'aspc', 'ahec', 'aajc', 'aszc', 'axzc', 'ampc', 'airc', 'adxc', 'antc', 'aonc', 'aelc', 'ajtc', 'anyc', 'azac', 'aslc', 'azmc', 'aiyc', 'artc', 'azjc', 'agwc', 'anfc', 'acxc', 'aaac', 'andc', 'amkc', 'ahbc', 'ahkc', 'anjc', 'ajzc', 'anvc', 'afuc', 'atgc', 'ahmc', 'akrc', 'aolc', 'aqic', 'ayac', 'awac', 'akxc', 'anuc', 'awgc', 'afgc', 'abjc', 'acfc', 'adrc', 'alic', 'aoyc', 'agrc', 'asnc', 'avzc', 'aogc', 'aywc', 'aqpc', 'atqc', 'ahgc', 'arqc', 'alvc', 'aczc', 'albc', 'auhc', 'amyc', 'amec', 'arkc', 'appc', 'asmc', 'acqc', 'aduc', 'akhc', 'aknc', 'adsc', 'arcc', 'amvc', 'apmc', 'avbc', 'awjc', 'aewc', 'aehc', 'aeuc', 'awvc', 'auvc', 'anzc', 'ayhc', 'alyc', 'amnc', 'afpc', 'adec', 'addc', 'ayyc', 'avdc', 'aoxc', 'alac', 'acoc', 'agdc', 'alnc', 'alqc', 'aiec', 'aqqc', 'ajdc', 'awoc', 'azuc', 'alhc', 'aupc', 'afqc', 'arhc', 'atoc', 'avyc', 'aruc', 'agxc', 'aepc', 'adic', 'aifc', 'ajoc', 'axjc', 'acdc', 'abmc', 'ajic', 'amrc', 'apuc', 'anwc', 'avoc', 'attc', 'adnc', 'alsc', 'ayfc', 'azzc', 'avrc', 'anoc', 'aqcc', 'acyc', 'aqgc', 'aeac', 'adlc', 'ayqc', 'acic', 'asoc', 'assc', 'abrc', 'ayic', 'aauc', 'atwc', 'astc', 'aghc', 'acgc', 'awbc', 'aqvc', 'aafc', 'avvc', 'agfc', 'afwc', 'ajqc', 'aokc', 'avjc', 'alec', 'anhc', 'aygc', 'axlc', 'agac', 'agoc', 'aqoc', 'ajmc', 'advc', 'aqfc', 'ahoc', 'aguc', 'aaic', 'aftc', 'avgc', 'autc', 'axyc', 'aebc', 'aquc', 'arlc', 'awfc', 'asuc', 'agmc', 'aezc', 'arsc', 'aicc', 'aikc', 'ahic', 'absc', 'awnc', 'acmc', 'acnc', 'aqec', 'adkc', 'asdc', 'adtc', 'axic', 'aggc', 'auic', 'ahpc', 'adjc', 'axrc', 'agec', 'afxc', 'aiac', 'akbc', 'auuc', 'anqc', 'aztc', 'ajvc', 'ausc', 'acjc', 'azqc', 'azkc', 'arpc', 'ahyc', 'aiwc', 'azgc', 'adbc', 'aalc', 'aeoc', 'aenc', 'axhc', 'asjc', 'aulc', 'aeec', 'agjc', 'aetc', 'apqc', 'altc', 'asfc', 'aqxc', 'aglc', 'asyc', 'ailc', 'aihc', 'ancc', 'abwc', 'aodc', 'azfc', 'afac', 'asac', 'ahwc', 'ajfc', 'ankc', 'aivc', 'anrc', 'aqnc', 'asic', 'aejc', 'amfc', 'azlc', 'asgc', 'abtc', 'anmc', 'aqtc', 'acpc', 'arac', 'aozc', 'atnc', 'aqyc', 'akgc', 'aprc', 'aytc', 'ajyc', 'aouc', 'aksc', 'aerc', 'agqc', 'ajwc', 'abcc', 'acrc', 'ahsc', 'ahfc', 'aljc', 'ajxc', 'aofc', 'agcc', 'aopc', 'aorc', 'atrc', 'atic', 'arrc', 'ayvc', 'abyc', 'ajgc', 'awmc', 'ahlc', 'agyc', 'afvc', 'akwc', 'aqjc', 'asxc', 'afic', 'afzc', 'ajjc', 'ainc', 'aiqc', 'aric', 'awic', 'alrc', 'aexc', 'argc', 'abhc', 'azdc', 'abpc', 'aqhc', 'aaqc', 'avac', 'acec', 'aizc', 'abqc', 'azcc', 'ahnc', 'atdc', 'aykc', 'atjc', 'axnc', 'ahuc', 'atlc', 'aaec', 'azpc', 'alwc', 'amjc', 'awkc', 'arec', 'agtc', 'aegc', 'avlc', 'aijc', 'aidc', 'aplc', 'acbc', 'apcc', 'apyc', 'abvc', 'azvc', 'agic', 'agnc', 'ahtc', 'ahac', 'avhc', 'akzc', 'aqdc', 'akfc', 'aucc', 'ahcc', 'amwc', 'ahxc', 'ajuc', 'auoc', 'aigc', 'aldc', 'aycc', 'apvc', 'audc', 'amac', 'angc', 'agbc', 'anic', 'admc', 'afrc', 'ajlc', 'awpc', 'agsc', 'abgc', 'apnc', 'abic', 'avnc', 'asec', 'adqc', 'aymc', 'aioc', 'aowc', 'apic', 'aqmc', 'azyc', 'aybc', 'akdc', 'azwc', 'aryc', 'aqac', 'apfc', 'alzc', 'atvc', 'aptc', 'adwc'})

    def test_edits_2_it(self):
        self.assertEqual(edits_2_it("abc"),{'adbc', 'azbc', 'arcb', 'abjc', 'abco', 'abxc', 'abfc', 'abac', 'abcx', 'avcb', 'axcb', 'alcb', 'afbc', 'abwc', 'aacb', 'ancb', 'albc', 'atbc', 'abcv', 'accb', 'ajbc', 'abnc', 'aabc', 'abci', 'apcb', 'abbc', 'abtc', 'avbc', 'abcd', 'atcb', 'abec', 'abca', 'abcw', 'abcg', 'abcr', 'aycb', 'abce', 'amcb', 'abcc', 'abcj', 'abcp', 'arbc', 'ahcb', 'abzc', 'aucb', 'ambc', 'aecb', 'abyc', 'adcb', 'akcb', 'asbc', 'abmc', 'abcz', 'abcu', 'ablc', 'abct', 'aboc', 'abcy', 'abhc', 'abpc', 'agcb', 'abcs', 'ajcb', 'aicb', 'aibc', 'axbc', 'abrc', 'abkc', 'abqc', 'abch', 'awbc', 'afcb', 'apbc', 'aqcb', 'abck', 'azcb', 'ascb', 'acbc', 'abvc', 'aubc', 'awcb', 'abdc', 'aebc', 'abcm', 'aobc', 'abcb', 'absc', 'anbc', 'abcn', 'abcl', 'agbc', 'akbc', 'abgc', 'abic', 'aocb', 'abcq', 'ahbc', 'aybc', 'abuc', 'abcf', 'aqbc'})

    def test_edits_2_ti(self):
        self.assertEqual(edits_2_ti("abc"),{'acyb', 'arcb', 'awcb', 'actb', 'aycb', 'acmb', 'acvb', 'acdb', 'amcb', 'abcb', 'aczb', 'acrb', 'agcb', 'acpb', 'acfb', 'aicb', 'acib', 'ajcb', 'acnb', 'avcb', 'alcb', 'axcb', 'aacb', 'ahcb', 'aclb', 'ancb', 'afcb', 'aqcb', 'acgb', 'accb', 'acab', 'aucb', 'acqb', 'acjb', 'aocb', 'azcb', 'aecb', 'ascb', 'aceb', 'acob', 'adcb', 'apcb', 'achb', 'acub', 'acbb', 'ackb', 'acxb', 'akcb', 'acwb', 'atcb', 'acsb'})



class testCandidates(unittest.TestCase):
    #candidates can validly return a 0 or any length list of strings.
    def setUp(self):
        self.maxDiff=None


    def test_candidates(self):
        self.assertListEqual(candidates("ther"),['the', 'tour', 'tsar', 'ter', 'tar', 'tor', 'tlr', 'tyr', 'tarr', 'torr'])

    def test_candidates_1_delete(self):
        s1 = candidates_1_delete("ther")
        s2 = {'ter': [0.45, 4.492768238117253e-06, 0.013296011196641007]}
        self.assertDictEqual(s1,s2)

    def test_candidates_1_insert(self):
        s1 = candidates_1_insert("ther")
        s2 = {'their': [0.15, 0.0024366322580970244, 0.014520643806857943], 'theer': [0.15, 1.9158926388559714e-08, 0.014520643806857943]}
        self.assertDictEqual(s1,s2)

    def test_candidates_1_substitute(self):
        s1 = candidates_1_substitute("ther")
        s2 = {'tier': [0.3, 3.7264111825748645e-06, 0.0], 'thor': [0.3, 4.885526229082727e-07, 0.016270118964310708], 'teer': [0.3, 3.831785277711943e-08, 0.0], 'thir': [0.3, 2.969633590226756e-07, 0.0155703289013296], 'thar': [0.3, 6.7056242359959e-08, 0.06787963610916725]}
        self.assertDictEqual(s1,s2)

    def test_candidates_1_transpose(self):
        self.assertDictEqual(candidates_1_transpose("ther"),{})

    def test_candidates_2_dd(self):
        self.assertDictEqual(candidates_2_dd("ther"),{})

    def test_candidates2_ii(self):
        s1 = candidates_2_ii("ther")
        s2 = {'tither': [0.075, 9.579463194279857e-09, 0.0], 'thenar': [0.075, 1.9158926388559714e-08, 0.0], 'tether': [0.075, 7.184597395709893e-07, 0.0], 'tocher': [0.075, 4.789731597139929e-08, 0.0], 'thaler': [0.075, 5.7476779165679146e-08, 0.0], 'tosher': [0.075, 9.579463194279857e-09, 0.0]}
        self.assertDictEqual(s1,s2)

    def test_candidates_2_ss(self):
        s1 = candidates_2_ss("ther")
        s2 = {'thor': [0.15, 4.885526229082727e-07, 0.0], 'tour': [0.15, 6.186417330865932e-05, 0.0], 'terr': [0.15, 2.8738389582839573e-08, 0.0], 'tsar': [0.15, 4.981320861025526e-06, 0.0], 'tier': [0.15, 3.7264111825748645e-06, 0.0], 'teer': [0.15, 3.831785277711943e-08, 0.0], 'tear': [0.15, 1.072899877759344e-05, 0.0], 'tarr': [0.15, 1.8200980069131728e-07, 0.0], 'thar': [0.15, 6.7056242359959e-08, 0.0], 'tzar': [0.15, 8.621516874851871e-08, 0.0], 'thir': [0.15, 2.969633590226756e-07, 0.0], 'tahr': [0.15, 2.8738389582839573e-08, 0.0], 'taur': [0.15, 9.579463194279857e-09, 0.0], 'torr': [0.15, 1.5327141110847771e-07, 0.0]}
        self.assertDictEqual(s1,s2)

    def test_candidates_2_tt(self):
        self.assertEqual(candidates_2_tt("ther"),{})

    def test_candidates_2_sd(self):
        s1 = candidates_2_sd("ther")
        s2 = {'tlr': [0.1875, 4.885526229082727e-07, 0.0], 'ter': [0.1875, 4.492768238117253e-06, 0.0], 'tur': [0.1875, 1.34112484719918e-07, 0.0], 'tor': [0.1875, 2.01168727079877e-06, 0.0], 'tfr': [0.1875, 9.579463194279857e-08, 0.0], 'tar': [0.1875, 2.279912240238606e-06, 0.0], 'tdr': [0.1875, 6.7056242359959e-08, 0.0], 'tyr': [0.1875, 2.490660430512763e-07, 0.0]}
        self.assertDictEqual(s1,s2)

    def test_candidates_2_ds(self):
        s1 = candidates_2_ds("ther")
        s2 = {'thor': [0.1875, 4.885526229082727e-07, 0.0], 'tour': [0.1875, 6.186417330865932e-05, 0.0], 'terr': [0.1875, 2.8738389582839573e-08, 0.0], 'tsar': [0.1875, 4.981320861025526e-06, 0.0], 'tier': [0.1875, 3.7264111825748645e-06, 0.0], 'teer': [0.1875, 3.831785277711943e-08, 0.0], 'tear': [0.1875, 1.072899877759344e-05, 0.0], 'tarr': [0.1875, 1.8200980069131728e-07, 0.0], 'thar': [0.1875, 6.7056242359959e-08, 0.0], 'tzar': [0.1875, 8.621516874851871e-08, 0.0], 'thir': [0.1875, 2.969633590226756e-07, 0.0], 'tahr': [0.1875, 2.8738389582839573e-08, 0.0], 'taur': [0.1875, 9.579463194279857e-09, 0.0], 'torr': [0.1875, 1.5327141110847771e-07, 0.0]}
        self.assertDictEqual(s1,s2)

    def test_candidates_2_dt(self):
        self.assertDictEqual(candidates_2_dt("ther"),{})

    def test_candidates_2_td(self):
        s1 = candidates_2_td("ther")
        s2 = {'ter': [0.1875, 4.492768238117253e-06, 0.0], 'the': [0.1875, 0.05789731759990803, 0.0]}
        self.assertDictEqual(s1,s2)

    def test_candidates_2_is(self):
        s1 = candidates_2_is("ther")
        s2 = {'titer': [0.1125, 9.579463194279857e-09, 0.0], 'tower': [0.1125, 3.1487695519597894e-05, 0.0], 'theer': [0.1125, 1.9158926388559714e-08, 0.0], 'tiger': [0.1125, 8.496983853326233e-06, 0.0], 'tyler': [0.1125, 2.5577166728727218e-06, 0.0], 'tahar': [0.1125, 9.579463194279857e-09, 0.0], 'trier': [0.1125, 5.556088652682317e-07, 0.0], 'tuber': [0.1125, 1.9158926388559715e-07, 0.0], 'their': [0.1125, 0.0024366322580970244, 0.0], 'tozer': [0.1125, 2.2990711666271659e-07, 0.0], 'tamer': [0.1125, 2.3948657985699645e-07, 0.0], 'tater': [0.1125, 1.9158926388559714e-08, 0.0], 'taper': [0.1125, 7.855159819309483e-07, 0.0], 'tayer': [0.1125, 9.579463194279857e-09, 0.0], 'taker': [0.1125, 5.556088652682317e-07, 0.0], 'truer': [0.1125, 9.196284666508663e-07, 0.0], 'tuner': [0.1125, 8.908900770680267e-07, 0.0], 'tiler': [0.1125, 3.1612228541123527e-07, 0.0], 'toper': [0.1125, 1.4369194791419785e-07, 0.0], 'tirer': [0.1125, 9.579463194279857e-09, 0.0], 'timer': [0.1125, 1.9254721020502513e-06, 0.0], 'toner': [0.1125, 9.866847090108252e-07, 0.0], 'taler': [0.1125, 5.7476779165679146e-08, 0.0], 'tiber': [0.1125, 2.68224969439836e-07, 0.0]}
        self.assertDictEqual(s1,s2)

    def test_candidates_2_ti(self):
        s1 = candidates_2_ti("ther")
        s2 = {'three': [0.1125, 0.0007501869216704442, 0.0], 'throe': [0.1125, 9.579463194279857e-09, 0.0], 'there': [0.1125, 0.003059556011231461, 0.0], 'thore': [0.1125, 2.8738389582839573e-08, 0.0]}
        self.assertDictEqual(s1,s2)

    def test_candidates_2_it(self):
        s1 = candidates_2_it("ther")
        s2 = {'their': [0.1125, 0.0024366322580970244, 0.0], 'there': [0.1125, 0.003059556011231461, 0.0], 'therm': [0.1125, 3.831785277711943e-08, 0.0], 'theer': [0.1125, 1.9158926388559714e-08, 0.0], 'thore': [0.1125, 2.8738389582839573e-08, 0.0]}
        self.assertDictEqual(s1,s2)

    def test_candidates_2_st(self):
        s1 = candidates_2_st("ther")
        s2 = {'tahr': [0.15, 2.8738389582839573e-08, 0.0], 'tore': [0.15, 6.284127855447586e-06, 0.0], 'tear': [0.15, 1.072899877759344e-05, 0.0], 'tyre': [0.15, 5.5273502630994776e-06, 0.0], 'terr': [0.15, 2.8738389582839573e-08, 0.0], 'thro': [0.15, 1.9158926388559715e-07, 0.0], 'tare': [0.15, 6.7056242359959e-08, 0.0], 'tire': [0.15, 1.5231346478904973e-06, 0.0], 'thru': [0.15, 6.514034972110303e-07, 0.0], 'teer': [0.15, 3.831785277711943e-08, 0.0]}
        self.assertDictEqual(s1,s2)

    def test_candidates_2_di(self):
        s1 = candidates_2_di("ther")
        s2 = {'tahr': [0.15, 2.8738389582839573e-08, 0.0], 'tear': [0.15, 1.072899877759344e-05, 0.0], 'thor': [0.15, 4.885526229082727e-07, 0.0], 'terr': [0.15, 2.8738389582839573e-08, 0.0], 'thar': [0.15, 6.7056242359959e-08, 0.0], 'tier': [0.15, 3.7264111825748645e-06, 0.0], 'teer': [0.15, 3.831785277711943e-08, 0.0], 'thir': [0.15, 2.969633590226756e-07, 0.0]}
        self.assertDictEqual(s1,s2)


class testConfusedWord(unittest.TestCase):

    def test_find_confused_word(self):
        self.assertEqual(find_confused_word("are"), "is")


class testConfusionMatricies(unittest.TestCase):

    def test_confusion_matrix_delete(self):
        self.assertEqual(confusion_matrix_delete("ct","cat"),0.006473058082575227)

    def test_confusion_matrix_insert(self):
        self.assertEqual(confusion_matrix_insert("cat","caet"),0.001224632610216935)

    def test_confusion_matrix_substitute(self):
        self.assertEqual(confusion_matrix_substitute("cat","cet"),0.05983205038488453)

    def test_confusion_matrix_transpose(self):
        self.assertEqual(confusion_matrix_transpose("cta","cat"),0.0006997900629811056)


#grammar check tests assume ngra type is trigram word
class testGrammarCheck(unittest.TestCase):
    

    def test_candidates_grammar_check(self):
        s1 = candidates_grammar_check("word")
        s2 = ['would', 'world', 'words', 'word', 'worked', 'wind', 'wood', 'wild', 'ward', 'wound', 'weird', 'wed', 'weed', 'wired', 'worded', 'wand', 'weld', 'wad', 'wold', 'wooed']
        self.assertListEqual(s1,s2, msg="Words that have the same probability may be swapped in the list due to floating point error,  results in a fail sometimes but is okay")

    def test_grammar_check(self):
        self.assertEqual(grammar_check("they is going skiing"),"word number 2, 'are' could be a better fit than 'is'")

    def test_get_quadgrams_p(self):
        if (get_current_ngram_type==NGRAM_TYPE_QUAD or get_current_ngram_type==NGRAM_TYPE_QUADPOS):
            self.assertEqual(get_quadgrams_p("the,first,time,i","the,first,time"), 0.05741439916677516)
        else:
            self.assertEqual(True, True)

    def test_get_trigrams_p(self):
        self.assertEqual(get_trigrams_p("the,first,time","the,first"), 0.12533655336716545)

    def test_get_bigrams_p(self):
        self.assertEqual(get_bigrams_p("the,first","the"), 0.010139644931252998)

    def test_get_unigrams_p(self):
        self.assertEqual(get_unigrams_p("the"), 0.05789731759990803)

    def test_dim_quadgram_score(self):
        self.assertEqual(dim_quadgram_score("the,first,time,i"), 0.08403355725966598)

    def test_get_sentence_probability(self):
        arg = [','.join(ng) for ng in ngrams("the first time i went to school i got lost".split(),4)]
        self.assertEqual(get_sentence_probability(arg), 3.3800508194502475e-10)

    def test_make_mixed_ngrams(self):
        arg = [','.join(ng) for ng in ngrams("the first time i went to school i got lost".split(),4)]
        resp = make_mixed_ngrams(arg)
        self.assertEqual(len(resp), 7)
        self.assertEqual(resp[3], 'PNP,went,TO0,school')

    def test_get_highest_val(self):
        highKey, highCandidate, highCount = get_highest_val(
                          {"a":{"b":0.1,"c":0.2}, "f":{"g":0.3,"h":0.4,"i":0.5}, "m":{"n":0.01} } )
        self.assertEqual(highKey, "f")
        self.assertEqual(highCandidate, "i")
        self.assertEqual(highCount, 2)

    def test_check_grammar(self):
        #check_grammar returns via suggest_replacement so only one test need
        self.assertEqual(grammar_check("I walk to the shop and I bought milk"),
                                    "word number 5, 'shops' could be a better fit than 'shop'")



if __name__ == '__main__':
    unittest.main()

