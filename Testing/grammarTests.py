from checkSpell import *
from checkGrammar import *

print("testings full ngram dictionaries\n")

print(("\nI walk to the shop and I bought milk. walk->walked"))
print(grammar_check("I walk to the shop and I bought milk"))

print(("\nThere has been some changes to the average grant limit over recent month. month->months"))
print(grammar_check("There has been some changes to the average grant limit over recent month"))

print(("\nwe will go to the park and then god back home. god->go"))
print(grammar_check("we will go to the park and then god back home"))

print(("\nWe all eat dinner and then made dessert. made->make"))
print(grammar_check("We all eat the fish and then made dessert"))


print(("\nmost of our business came from weddings receptions. weddings->wedding"))
print(grammar_check("most of our business came from weddings receptions."))


print(("\nWhen you are done with that lab report, can you send it to Bill and me? me->i"))
print(grammar_check("When you get done with that lab report, can you send it to Bill and me?"))

print(" \ncarbon monoxide is a colourless, odourless, tasteless gas that can cause serious injury or even death if inhaled in high quantities.-no change")
print(grammar_check(" carbon monoxide is a colourless, odourless, tasteless gas that can cause serious injury or even death if inhaled in high quantities.- no change"))



print(" \nwith your permission we and our partners may use data and identification through device scanning.-no change")
print(grammar_check(" with your permission we and our partners may use data and identification through device scanning."))



print(" \nalternatively you may access more detailed information and change your preferences before consenting or to refuse consenting.-no change")
print(grammar_check(" alternatively you may access more detailed information and change your preferences before consenting or to refuse consenting."))



print(" \nwith the election taking place during the coronavirus pandemic,he said: I think even people who don't like Sturgeon, for whatever reason, have had to admit that she has been by far the most competent leaders on these islands dealing with this crisis.- change word 37 'leaders'-> 'leader'")
print(grammar_check(" with the election taking place during the coronavirus pandemic, he said: I think even people who don't like Sturgeon, for whatever reason, have had to admit that she has been by far the most competent leaders on these islands dealing with this crisis."))



print("\nI got a pizza with my boyfriend and then watched television-no change")
print(grammar_check("I got a pizza with my boyfriend and then watched television-no change"))


print("\nShe went downstairs and found her sofa was missing.-no change")
print(grammar_check("She went downstairs and found her sofa was missing"))



print(" \nMillions rely on the Guardian for independent journalism that stands for truth and integrity. - no change")
print(grammar_check(" Millions rely on the Guardian for independent journalism that stands for truth and integrity."))


print(("\nshe was pleased to meat her. meat->meet"))
print(grammar_check("she was pleased to meat her"))

print(("\ntwo pea in a pot"))
print(grammar_check("two pea in a pot"))

print(("\ntwo peas in a pot is a common expression. pot->pod"))
print(grammar_check("two peas in a pot is a common expression"))

print(("\nthere car is over there. 1st there->their"))
print(grammar_check("there car is over their"))

print(("\npea in a pod - common phrase, incorrect tense. pea->peas"))
print(grammar_check("pea in a pod"))

print(("\nshe see her home - see->saw/sees"))
print(grammar_check("she see her home"))


print(("\nshe wept to school. - wept -> went'"))
print(grammar_check("she wept to school"))

print(("\nshe drawn a bath - 'drawn' -> 'drew'"))
print(grammar_check("she drawn a bath"))

print(("\nThe work was completed efficiently and to standard - 'no change'"))
print(grammar_check("work was completed efficiency"))

print(("\nputting the wasting away - 'wasting' -> 'washing'"))
print(grammar_check("putting the wasting away"))

print(("\ncurtains were draw shut - 'draw' -> 'drawn'"))
print(grammar_check("curtains were draw shut"))

print(("\nwe ordered tie food, it was lovely. tie -> the"))
print(grammar_check("we ordered tie food, it was lovely"))

print(("\nI can't hardly believe what she said. can't -> can"))
print(grammar_check("I can't hardly believe what she said"))

print(("\nI don't want no pudding. -> no -> any"))
print(grammar_check("I don't want no pudding."))

print(("\nthey ate going skiing. is -> are"))
print(grammar_check("they ate going skiing"))

print(("\ni was wondering how many of the sweets are left. -no change"))
print(grammar_check("i was wondering how many of the sweets are left"))

print(("\nWe enjoys horror movies. enjoys -> enjoy"))
print(grammar_check("We enjoys horror movies.'"))

