import shelve

import os.path

from nltk import sent_tokenize
from nltk import word_tokenize
from nltk.util import ngrams

import checkSpell
from checkSpell import is_spelled_correctly
from checkSpell import candidates_grammar_check
from checkSpell import WORDS
from checkSpell import WORD_POS




def tokenise_sentences(text):
    sentences = sent_tokenize(text)
    return sentences


def word_is_propernoun(word):
    ''' True if word is propernoun, based of Part Of Speach information in the dictionary'''
    if (word in WORDS and WORDS[word][WORD_POS]=='NP0'):
        return True
    else:
        return False


#constants for selecting ngram type
NGRAM_TYPE_QUAD = 4
NGRAM_TYPE_QUADPOS = 3
NGRAM_TYPE_TRI = 2
NGRAM_TYPE_TRIPOS = 1
#initial ngram type is initialisd below the set_ngram_type function
QUADGRAM_WORD_LIST=None
TRIGRAM_WORD_LIST=None
BIGRAM_WORD_LIST=None
UNIGRAM_WORD_LIST=None
POSyes = False
_current_ngram_type=None

def get_current_ngram_type():
    return _current_ngram_type


def set_ngram_type(ngram_type):
    '''allows user to choose one of 4 configurations, Quadgram word/quadgram POS/trigram word/trigram POS'''
    global QUADGRAM_WORD_LIST, TRIGRAM_WORD_LIST, BIGRAM_WORD_LIST, UNIGRAM_WORD_LIST, POSyes, _current_ngram_type
    if (QUADGRAM_WORD_LIST!=None):
        QUADGRAM_WORD_LIST.close()
    if (TRIGRAM_WORD_LIST!=None):
        TRIGRAM_WORD_LIST.close()
    if (BIGRAM_WORD_LIST!=None):
        BIGRAM_WORD_LIST.close()
    if (UNIGRAM_WORD_LIST!=None and isinstance(UNIGRAM_WORD_LIST, shelve.DbfilenameShelf)):
        UNIGRAM_WORD_LIST.close()
    QUADGRAM_WORD_LIST=None
    TRIGRAM_WORD_LIST=None
    BIGRAM_WORD_LIST=None
    UNIGRAM_WORD_LIST=None
    _current_ngram_type=ngram_type
    if (_current_ngram_type == NGRAM_TYPE_QUAD):
        if (os.path.exists("Dict4gramShort.db")):   #check for missing quadgram file
            QUADGRAM_WORD_LIST = shelve.open("Dict4gramShort.db", "r")
        TRIGRAM_WORD_LIST = shelve.open("Dict3gramShort.db", "r")
        BIGRAM_WORD_LIST = shelve.open("Dict2gram.db", "r")
        UNIGRAM_WORD_LIST = WORDS
        POSyes = False
    elif (_current_ngram_type == NGRAM_TYPE_QUADPOS):
        if (os.path.exists("Dict4gramPOS.db")):   #check for missing quadgram file
            QUADGRAM_WORD_LIST = shelve.open("Dict4gramPOS.db", "r")
        TRIGRAM_WORD_LIST = shelve.open("Dict3gramPOS.db", "r")
        BIGRAM_WORD_LIST = shelve.open("Dict2gramPOS.db", "r")
        UNIGRAM_WORD_LIST = shelve.open("Dict1gramPOS.db", "r")  
        POSyes = True     
    elif (_current_ngram_type == NGRAM_TYPE_TRI):
        QUADGRAM_WORD_LIST = None
        TRIGRAM_WORD_LIST = shelve.open("Dict3gramShort.db", "r")
        BIGRAM_WORD_LIST = shelve.open("Dict2gram.db", "r")
        UNIGRAM_WORD_LIST = WORDS  
        POSyes = False 
    elif (_current_ngram_type == NGRAM_TYPE_TRIPOS):
        QUADGRAM_WORD_LIST = None
        TRIGRAM_WORD_LIST = shelve.open("Dict3gramPOS.db", "r")
        BIGRAM_WORD_LIST = shelve.open("Dict2gramPOS.db", "r")
        UNIGRAM_WORD_LIST = shelve.open("Dict1gramPOS.db", "r") 
        POSyes = True  


#initalise initail ngram type
set_ngram_type(NGRAM_TYPE_TRI)



global SHORT_SENTENCE
SHORT_SENTENCE = 1
global LONG_SENTENCE
LONG_SENTENCE = 2
global STANDARD_SENTENCE
STANDARD_SENTENCE = 0

def suggest_replacement(wordProbabilities, basicSentenceProbability, sentenceFlag):
    '''gets information of sentence probabilities and chooses a suggestion'''
    if (sentenceFlag == SHORT_SENTENCE):
        return("Short sentence skipped")
    [highKey, highCandidate, highCount] = get_highest_val(wordProbabilities)
    ratio = wordProbabilities[highKey][highCandidate]/basicSentenceProbability 
    threshold = 1.1
    if (highKey!=None and ratio > threshold and sentenceFlag == LONG_SENTENCE):
        suggestion = ("word number " + str(highCount) + ", '" + highCandidate + "', could be a better fit than '" + highKey +"', also the current sentence is too complex, shorten or break into more clauses")
        return suggestion
    elif (highKey!=None and ratio > threshold):
        suggestion = ("word number " + str(highCount) + ", '" + highCandidate + "' could be a better fit than '" + highKey + "'")
        return suggestion
    else:
        return None

def get_highest_val(probabilityDict):
    '''searches sentence probability dictionary created in grammar check to find word that created the highest probability sentence'''
    highestProbabilityValue=0
    countKey=0
    highKey = None
    highCandidate = None
    highCount = None
    for key in probabilityDict.keys():
        countKey +=1
        for value in probabilityDict[key]:
            if probabilityDict[key][value]>highestProbabilityValue:
                highestProbabilityValue = probabilityDict[key][value]
                highKey=key
                highCandidate=value   
                highCount=countKey  
    return highKey, highCandidate, highCount


def grammar_check(text):
    '''main grammar checking function, organises checking and returns suggestion'''
    probabilityDict={}
    DoubleAverageSentenceLength=2*WORDS["AverageSentenceLength"][0]
    sentence = text.lower()
    basicSentenceProbability= 0
    sentenceFlag=STANDARD_SENTENCE
    unigrams = [unigram for unigram in word_tokenize(sentence) if unigram.isalpha()]      
    #check sentence spelling
    count=-1    
    for word in unigrams:
        count+=1
        if (checkSpell.is_spelled_correctly(word) == False):
            return "word number "+str(count)+", '"+word+"', is not recognised. Please check spelling."
    #check complexity
    commaCount = sentence.count(",")
    if (len(unigrams)>=DoubleAverageSentenceLength and commaCount <= 1):
        sentenceFlag=LONG_SENTENCE
    elif(len(unigrams) < 4):
        return suggest_replacement(None, None, SHORT_SENTENCE)
    ngramList = [','.join(ng) for ng in ngrams(unigrams,4)]
    if (POSyes): #if using POS, convert to mixed ngram list
        ngramList=make_mixed_ngrams(ngramList)
    basicSentenceProbability = get_sentence_probability(ngramList)
    count=-1
    for word in unigrams:
        #for each word, get matching alternatives and calculate probability of sentence if it were used
        probabilityDict[word]={} #
        count+=1
        confusionCandidates=candidates_grammar_check(word)
        newSentenceList = list(unigrams)
        for candidate in confusionCandidates:
            #make new sentence with candidate and get ngrams
            if candidate != word:
                newSentenceList[count] = candidate
                newngrams = [','.join(ng) for ng in ngrams(newSentenceList,4)]
                if (POSyes):
                    newngrams=make_mixed_ngrams(newngrams)
                probabilityDict[word][candidate]=get_sentence_probability(newngrams)
    return suggest_replacement(probabilityDict, basicSentenceProbability, sentenceFlag)


def get_sentence_probability(ngrams):
    '''multiply the dim probabilties for the sentence probabilty'''
    sentenceP=1
    for ngram in ngrams:
        contextP = dim_quadgram_score(ngram)
        sentenceP = contextP*sentenceP
    return sentenceP


#general score of the probablity of a trigram (a,b,c) using maximum likelihood estimate, splits into
#trigram (a,b,c), bigram (a,b) and unigram(a), to estimate probability.

#check for a minimum probabilty value, chaining weighted n-grams from tri to uni, giving most weigt to trigram as that is best at context
#unigram ensures P>0 but doesnt offer much help with context checking, so a minimum likelihood threshold is used to detect errors accurately while minimising real world errors due to sparseness of data

def dim_quadgram_score(quadgram):
    '''calculate the deleted interpolation probability of the highest order ngram'''
    trigram = quadgram.rsplit(',', 1)[0]
    bigram = quadgram.rsplit(',', 2)[0]
    unigram = quadgram.rsplit(',', 3)[0]
    contextP=0
    if (QUADGRAM_WORD_LIST != None):
        contextP = 0.5*get_quadgrams_p(quadgram,trigram) + 0.3*get_trigrams_p(trigram,bigram) + 0.15*get_bigrams_p(bigram,unigram)+0.05*get_unigrams_p(unigram)
    elif (TRIGRAM_WORD_LIST != None):
        contextP = 0.6*get_trigrams_p(trigram,bigram) + 0.3*get_bigrams_p(bigram,unigram)+0.1*get_unigrams_p(unigram)
    else:
        contextP = 0.7*get_bigrams_p(bigram,unigram)+0.3*get_unigrams_p(unigram)
    return contextP



SMALLEST_RETURN_PROBABILITY=0.00000000001

def get_quadgrams_p(quadgram,trigram):
    '''gets probabilty of a quadgram by dividing frequency by its trigram frequency'''
    quadgramP=SMALLEST_RETURN_PROBABILITY
    if (quadgram in QUADGRAM_WORD_LIST):
        quadgramP = (QUADGRAM_WORD_LIST[quadgram][0])/(TRIGRAM_WORD_LIST[trigram][0])
    return quadgramP

def get_trigrams_p(trigram,bigram):
    '''gets probabilty of a trigram by dividing frequency by its bigram frequency'''
    trigramP=SMALLEST_RETURN_PROBABILITY
    if (trigram in TRIGRAM_WORD_LIST):
        trigramP = (TRIGRAM_WORD_LIST[trigram][0])/(BIGRAM_WORD_LIST[bigram][0])
    return trigramP

def get_bigrams_p(bigram,unigram):
    '''gets probabilty of a bigram by dividing frequency by its unigram frequency'''
    bigramP=SMALLEST_RETURN_PROBABILITY
    if (bigram in BIGRAM_WORD_LIST):
        bigramP = BIGRAM_WORD_LIST[bigram][0]/UNIGRAM_WORD_LIST[unigram][0]
    return bigramP

def get_unigrams_p(unigram):
    '''gets probabilty of a unigram by dividing frequency by its dictionary total'''
    unigramP=SMALLEST_RETURN_PROBABILITY
    if (unigram in UNIGRAM_WORD_LIST):
        unigramP=UNIGRAM_WORD_LIST[unigram][0] / UNIGRAM_WORD_LIST["WordFrequencyTotal"][0]
    return unigramP

def make_mixed_ngrams(ngrams):
    '''makes mixed quadgrams from word quadrams'''
    mixedNgrams=[]
    for ngram in ngrams:
        words=ngram.split(',')
        mixedNgrams.append(WORDS[words[0]][1] +','+ words[1] +','+ WORDS[words[2]][1] +','+ words[3])
    return mixedNgrams



#prototype of accept/reject functions for learning. lef as comment as they are a possible future development
'''
def accept(sentence,suggestion,sentencePos):
    trigram = sentence[sentencePos-1]+','+suggestion+','+sentence[sentencePos+1]
    if trigram in trigramWordlist:
        trigramWordList[trigram]+=1
    else:
        trigramWordList.update(trigram)


def reject(sentence,suggestion,sentencePos):
    trigram = sentence[sentencePos-1]+sentence[sentencePos]+sentence[sentencePos+1]
    if trigram in trigramWordlist:
        trigramWordList[trigram]+=1
    else:
        trigramWordList.update(trigram)
'''
