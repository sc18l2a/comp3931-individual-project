
import tkinter
import tkinter.ttk
import tkinter.tix
import tkinter.filedialog
import tkinter.messagebox

import checkSpell
import checkGrammar

from checkGrammar import tokenise_sentences
from checkGrammar import grammar_check


global _tk_root
global _tk_text_frame
global _tk_status_frame
global _tk_grammar_frame
global _tk_grammar_box
global _tk_grammar_button
global _tk_grammar_type_select
global _tk_spelling_button
global _tk_load_button
global _tk_saveas_button
global _tk_save_button
global _tk_menu_frame

global _grammar_sentences
_grammar_sentences = []

'''set of correcly spelled words in the current document'''
global _collected_words
_collected_words=set()

'''list of characters that signify the end of a word, used to trigger the checking of a completed word'''
global WORD_ENDINGS
WORD_ENDINGS = ('\n',' ','.',',',';',':','?','!')

'''used to set unique tagname for tkinter.text.mark'''
global tagseq
tagseq=1

'''list of misspelled words that should be ignored within the current document'''
global ignored_words
ignored_words=set()

global filename
filename=""


def load_selected():
    '''Load _tk_text_frame from specified file.''' 
    global _tk_text_frame
    global ignored_words
    #select, open and read file
    loadfilename=tkinter.filedialog.askopenfilename()
    if (len(loadfilename)==0):
            return
    global filename  
    filename=loadfilename      
    file1=open(filename, mode="r")   
    body=file1.read()
    #check to see if this file contains the SPELLER header
    if (body[0:7]=="SPELLER"):
        #extract header and body
        header,body=body.split("\n---\n")
        #check header data
        headerLines=header.splitlines()
        for headerInstruction in headerLines:
            headerInstructionItems=headerInstruction.split(':')
            if (headerInstructionItems[0] == "Ignore"):
                ignored_words.update(headerInstructionItems[1:])
    #put document body into the _tk_text_frame
    _tk_text_frame.delete(1.0, tkinter.END)
    _tk_text_frame.insert(1.0, body)
    full_spellcheck()
    _tk_text_frame.edit_modified(False)
    file1.close()
    global _tk_root
    _tk_root.title("Spellchecker - "+filename)


def select_filename():
    '''Select a filename to save to, using standard dialog.'''
    global filename
    origfilename=filename
    filename=tkinter.filedialog.asksaveasfilename(title = "Select file")
    if (len(filename)==0):
        filename=origfilename
        return False
    else:
        return True


def saveas_selected():
    '''Clears current filename before calling save_selected.'''
    global filename
    filename=""
    save_selected()

def save_selected():
    '''Save contents of _tk_text_frame, selecting filename if not already set.'''
    global filename
    if (filename==""):
        if (select_filename()==False):
            return
    file1=open(filename, mode="w")
    global _tk_text_frame
    #write header
    file1.write("SPELLER\n")
    if (len(ignored_words) > 0):
        file1.write("Ignore")
        for word in ignored_words:
            file1.write(":"+word)
        file1.write("\n")
    file1.write("---\n")
    #write body
    t = _tk_text_frame.get("1.0", "end-1c")
    file1.write(t)
    _tk_text_frame.edit_modified(False)
    file1.close()
    global _tk_root
    _tk_root.title("Spellchecker - "+filename)

def exit_selected():
    global _tk_text_frame
    if not _tk_text_frame.edit_modified():
        exit()
    else:
        if (tkinter.messagebox.askyesno('QUIT', 'Contents of editor not saved, are you sure you want to Quit?')):
            exit()
        else:
            return



def rtn_press(event):
    '''call word() when <rtn> is pressed'''
    word()

def key_press(event):
    '''called after every time a key is pressed, call word() if keypress is a word ending'''
    global WORD_ENDINGS
    if (event.char in WORD_ENDINGS):
        word()

def word():
    '''word to left of cursor ready to be checked for spelling, capitalisaton and duplication'''
    global _tk_text_frame
    #set end_pos as current cursor position and start_pos using tkinter indexing
    end_pos=_tk_text_frame.index(tkinter.INSERT)
    start_pos=_tk_text_frame.index(end_pos+"-1c")
    #step back to start of word
    while (start_pos!="1.0" and _tk_text_frame.get(start_pos) not in WORD_ENDINGS):
         start_pos = _tk_text_frame.index(start_pos+"-1c")
    #one step forward again if not start of text
    if (start_pos!="1.0" or (start_pos=="1.0" and _tk_text_frame.get(start_pos) in WORD_ENDINGS)):
        start_pos = _tk_text_frame.index(start_pos+"+1c")
    #if start_pos on the line above end_pos, its because the line wordstart reads past '\n'! set start_pos to start of line
    if (int(start_pos.split('.')[0]) < int(end_pos.split('.')[0])):
        start_pos=end_pos.split('.')[0] + ".0"
    word=_tk_text_frame.get(start_pos, end_pos)
    if (spell_check(word, start_pos, end_pos) == True):
        #check to see if word is propernoun and capitalise first letter
        if (checkGrammar.word_is_propernoun(word)):
            _tk_text_frame.replace(_tk_text_frame.index(start_pos), _tk_text_frame.index(start_pos+"+1c"), word[0].upper())
        #perform other checks
        check_capitalise_word_at_sentence_start(word, start_pos, end_pos)        
        check_duplicate_consecutive_words(word, start_pos, end_pos)
        global _collected_words
        _collected_words.add(word)        
        checkSpell.register_collected_words(_collected_words)




def spell_check(word, start_pos, end_pos):
    '''perform spellcheck, arranging for word to be marked if misspelled'''
    if ((word=='') or (word in WORD_ENDINGS)): #check if word is empty (due to pos being "1.0") or word is punctuation
        return False #skip as there is no word to the left of this punctuation 
    if (checkSpell.is_spelled_correctly(word) or word in ignored_words):
        return True 
    else:
        mark_misspelled_word(word, start_pos, end_pos)
        return False

def check_capitalise_word_at_sentence_start(word, start_pos, end_pos):
    '''search back looking for sentence punctuation before previous word'''
    prev_start_pos=_tk_text_frame.index(start_pos+"-1c")
    while (_tk_text_frame.get(prev_start_pos) in WORD_ENDINGS and prev_start_pos!="1.0"):
        prev_start_pos=_tk_text_frame.index(prev_start_pos+"-1c")
        if (_tk_text_frame.get(prev_start_pos) in ['.', '?', '!']):
            #sentence punctuation found - Uppercase first letter of this word
            _tk_text_frame.replace(start_pos, _tk_text_frame.index(start_pos+"+1c"), word[0].upper())

def check_duplicate_consecutive_words(word, start_pos, end_pos):
    '''look at previous word and mark this one if they are the same'''
    prev_word, unused1, unused2 = tktext_previous_word(start_pos, end_pos)
    if (prev_word == word):
        mark_duplicate_word(word, start_pos, end_pos)




def mark_misspelled_word(word, start, end):
    '''Create a tkinter text Tag between start and end and bind a right-click event handler
    to the Tag that will call show_replacement_words.'''
    global _tk_text_frame
    #use word as tagseq as it can the be applied to all instances of the misspelling and all fixed as once by replace_word
    strtag=word 
    _tk_text_frame.tag_add(strtag, start, end)
    _tk_text_frame.tag_config(strtag, background="yellow", underline="true")
    _tk_text_frame.tag_bind(strtag, "<Button-3>", show_replacement_words)

def mark_duplicate_word(word, start, end):
    '''Create a tkinter text Tag between start and end and bind a right-click event handler
    to the tag that will delete the tagged word.'''
    global tagseq
    global _tk_text_frame
    #use a unique id for this Tag
    strtag=str(tagseq)
    _tk_text_frame.tag_add(strtag, start, end)
    _tk_text_frame.tag_config(strtag, background="yellow", underline="true")
    #additional variable ev needed as binding to a mouse event forces the event object into the first function parameter. This does not happen when bound to a menu event
    _tk_text_frame.tag_bind(strtag, "<Button-3>", lambda ev=0, t=strtag, s=start,e=end : show_duplicate_word_options(ev, t, s, e))
    tagseq=tagseq+1




def show_replacement_words(event):
    '''Misspelt Word Event - When right click event on a marked word, find the word in the Tag and, if not spelled correctly,
    get candidate replacements.'''
    #get text position of click
    global _tk_text_frame    
    click_pos=_tk_text_frame.index("@%d,%d" % (event.x, event.y))
    #get the word, start and end of the word that was clicked on  
    word, start_pos, end_pos = tktext_current_word(click_pos)
    replacements=checkSpell.candidates(word)
    #create popup menu
    m = tkinter.Menu(_tk_text_frame, tearoff=0)
    for replacement in replacements:
        #https://stackoverflow.com/questions/728356/dynamically-creating-a-menu-in-tkinter-lambda-expressions
        m.add_command(label=replacement, command=lambda s=start_pos,e=end_pos,r=replacement : replace_word(s, e, r))
    m.add_separator()
    m.add_command(label="Ignore in this document", command=lambda w=word : ignore_in_doc(w))
    m.tk_popup(event.x_root, event.y_root) 

def show_duplicate_word_options(event, tag, start, end):
    '''Duplicate Word Event - show popup menu for a word marked as duplicate'''
    global _tk_text_frame
    #take one step back to suit tktext delete function
    start=_tk_text_frame.index(start+"-1c")
    #show menu
    m = tkinter.Menu(_tk_text_frame, tearoff=0)
    m.add_command(label="Duplicate word", state=tkinter.DISABLED)
    m.add_separator()
    m.add_command(label="Keep", command=lambda t=tag : _tk_text_frame.tag_delete(t))
    m.add_command(label="Delete", command=lambda s=start,e=end : _tk_text_frame.delete(s, e))
    m.tk_popup(event.x_root, event.y_root)



def replace_word(replacement_start, replacement_end, word):
    '''Replacement Word Menu Event - replace text indexed by start and end with word'''
    _tk_text_frame.delete(replacement_start, replacement_end)
    _tk_text_frame.insert(replacement_start, word)

def ignore_in_doc(word):
    '''Replacement Words Menu Event - Add word to ignore list and remove tag marking it'''
    global ignored_words
    ignored_words.add(word)
    #delete tag for this word
    _tk_text_frame.tag_delete(word)




def full_spellcheck():
    '''Spellcheck Button Event - Run spell check on each word in _tk_text_frame.'''
    #delete all current tags
    global _tk_text_frame
    for tag in _tk_text_frame.tag_names(index=None):
        _tk_text_frame.tag_remove(tag, "1.0", "end")
    #for all words, call spell_check, updating collected words for each that is correct
    next_word, start_pos, end_pos = tktext_first_word()
    global _collected_words
    while (next_word != ''):
        if (spell_check(next_word, start_pos, end_pos) == True):
            _collected_words.add(word)
        next_word, start_pos, end_pos = tktext_next_word(start_pos, end_pos)
    checkSpell.register_collected_words(_collected_words)




'''mapping between names using in the UI and type id used in checkGrammar. Used to get id from name and also name from id.'''
_NGRAM_TYPE = {"Quadgrams":checkGrammar.NGRAM_TYPE_QUAD, "Quadgrams with POS":checkGrammar.NGRAM_TYPE_QUADPOS,
               "Trigrams":checkGrammar.NGRAM_TYPE_TRI, "Trigrams with POS":checkGrammar.NGRAM_TYPE_TRIPOS}

def grammar_method_changed(event):
    '''bound to UI selector in grammar check, passes message to grammar check module to switch between different ngram configurations'''
    global _tk_grammar_type_select
    option=_tk_grammar_type_select.get()
    checkGrammar.set_ngram_type(_NGRAM_TYPE[option])
    #restart grammar check after changing ngram type
    grammar_selected()

def grammar_next_selected():
    '''bound to grammar next button, process next sentence.'''
    #send next sentence to grammarCheck
    global _grammar_sentences, _tk_grammar_box
    if (_grammar_sentences != None):
        if (len(_grammar_sentences) > 0):
            grammar_sentence=_grammar_sentences.pop(0)
            grammarCheckResult=checkGrammar.grammar_check(grammar_sentence)
            if (grammarCheckResult != None):
                _tk_grammar_box.configure(text=grammar_sentence+'\n'+grammarCheckResult)
            else:
                _tk_grammar_box.configure(text=grammar_sentence+'\n'+"No issues found")
        else:
            _tk_grammar_box.configure(text="Last sentence checked")
            _grammar_sentences=None
    else:
        grammar_stop_selected()

def grammar_stop_selected():
    '''bound to grammar stop button, clears variables and hides the grammar UI controls.'''
    #clear sentence list and grammar box
    global _grammar_sentences
    _grammar_sentences=None
    _tk_grammar_box.configure(text="")
    #hide _tk_grammar_box
    global _tk_grammar_frame
    _tk_grammar_frame.pack_forget()
    _tk_status_frame.pack_forget()
    #enable buttons
    global _tk_grammar_button, _tk_load_button, _tk_saveas_button, _tk_save_button, _tk_spelling_button
    _tk_grammar_button.configure(state=tkinter.NORMAL)
    _tk_load_button.configure(state=tkinter.NORMAL)
    _tk_saveas_button.configure(state=tkinter.NORMAL)
    _tk_save_button.configure(state=tkinter.NORMAL)
    _tk_spelling_button.configure(state=tkinter.NORMAL)

def grammar_selected():
    '''bound to the Grammar button, shows the grammar UI, gets the current text and process the first sentence.'''
    #show grammar box
    global _tk_grammar_frame, _tk_status_frame, _tk_menu_frame
    _tk_status_frame.pack(fill=tkinter.BOTH, expand=True, before=_tk_menu_frame)
    _tk_grammar_frame.pack(fill=tkinter.BOTH, expand=True)
    #disable most main buttons during grammar check
    global _tk_grammar_button, _tk_load_button, _tk_saveas_button, _tk_save_button, _tk_spelling_button
    _tk_grammar_button.configure(state=tkinter.DISABLED)
    _tk_load_button.configure(state=tkinter.DISABLED)
    _tk_saveas_button.configure(state=tkinter.DISABLED)
    _tk_save_button.configure(state=tkinter.DISABLED)
    _tk_spelling_button.configure(state=tkinter.DISABLED)
    #convert full text into list of sentences
    global _grammar_sentences, _tk_grammar_box
    _grammar_sentences = checkGrammar.tokenise_sentences(_tk_text_frame.get("1.0", "end"))
    #now put an initial message into the grammar box
    _tk_grammar_box.configure(text="Full grammar check. Select 'Next' to see recommendations for each sentence.")




'''
Four Tk Text helper functions - used as this editor needs specific behaviour at line breaks and
makes the code that needs to step through words much simpler and easier to maintain.
'''
def tktext_current_word(mid_pos):
    '''return start & end indexes of the current (with mid_pos) word'''
    start_pos=mid_pos
    #step back to start of word
    while (start_pos!="1.0" and _tk_text_frame.get(start_pos) not in WORD_ENDINGS):
         start_pos = _tk_text_frame.index(start_pos+"-1c")
    #one step forward again if not start of text
    if (start_pos!="1.0" or (start_pos=="1.0" and _tk_text_frame.get(start_pos) in WORD_ENDINGS)):
        start_pos = _tk_text_frame.index(start_pos+"+1c")
    #step forward to end of word
    end_pos=mid_pos
    end_index = _tk_text_frame.index("end")
    while (end_pos!=end_index and _tk_text_frame.get(end_pos) not in WORD_ENDINGS):
         end_pos = _tk_text_frame.index(end_pos+"+1c")
    #send back result
    return _tk_text_frame.get(start_pos, end_pos), start_pos, end_pos

def tktext_first_word():
    '''return word & start & end indexes of the first word in the text'''
    global _tk_text_frame
    charcount=0
    while (_tk_text_frame.get("1.0+"+str(charcount)+"c") in WORD_ENDINGS):
        charcount+=1
    #return start and end index positions
    start_pos=_tk_text_frame.index("1.0+"+str(charcount)+"c")
    end_pos=_tk_text_frame.index(start_pos+" wordend")
    return _tk_text_frame.get(start_pos, end_pos), start_pos, end_pos

def tktext_previous_word(start_pos, end_pos):
    '''return word & start_pos & end_pos for the previous word on the same line'''
    global _tk_text_frame
    line_numb=start_pos.split('.')[0]
    prev_end_pos=_tk_text_frame.index(start_pos+"-1c")
    while (prev_end_pos.split('.')[0]==line_numb and prev_end_pos != "1.0" and _tk_text_frame.get(prev_end_pos) in WORD_ENDINGS):
        prev_end_pos=_tk_text_frame.index(prev_end_pos+"-1c")
    if (prev_end_pos.split('.')[0]!=line_numb or prev_end_pos=="1.0"):
        return '', 0, 0 #search past start of line OR got to the start of the text, so do nothing
    else:
        #end of previous word found, now find start
        prev_start_pos=prev_end_pos
        prev_end_pos=_tk_text_frame.index(prev_end_pos+"+1c")
        while (prev_start_pos.split('.')[0]==line_numb and prev_start_pos!="1.0" and _tk_text_frame.get(prev_start_pos) not in WORD_ENDINGS):
            prev_start_pos=_tk_text_frame.index(prev_start_pos+"-1c")
        #move forward again, unless at the start of text
        if (prev_start_pos!="1.0"):
            prev_start_pos=_tk_text_frame.index(prev_start_pos+"+1c")
    return _tk_text_frame.get(prev_start_pos, prev_end_pos), prev_start_pos, prev_end_pos

def tktext_next_word(start_pos, end_pos):
    '''return word & start_pos & end_pos for the next word'''
    global _tk_text_frame
    end_index = _tk_text_frame.index("end")
    next_start_pos=_tk_text_frame.index(end_pos+"+1c")
    while (next_start_pos != end_index and _tk_text_frame.get(next_start_pos) in WORD_ENDINGS):
        next_start_pos=_tk_text_frame.index(next_start_pos+"+1c")
    if (next_start_pos==end_index):
        return '', 0, 0 #search past start of line OR got to the start of the text, so do nothing
    else:
        #start of next word found, now find end
        next_end_pos=next_start_pos
        while (next_end_pos!=end_index and _tk_text_frame.get(next_end_pos) not in WORD_ENDINGS):
            next_end_pos=_tk_text_frame.index(next_end_pos+"+1c")
        #move back again if word is at end of text
        if (next_start_pos==end_index):
            next_start_pos=_tk_text_frame.index(next_start_pos+"-1c")
    return _tk_text_frame.get(next_start_pos, next_end_pos), next_start_pos, next_end_pos




def main():
    #Create a root window with a main frame area
    global _tk_root
    _tk_root = tkinter.Tk()
    _tk_root.title("Spellchecker")
    frame1 = tkinter.Frame(_tk_root, relief=tkinter.RAISED, borderwidth=1)
    frame1.pack(fill=tkinter.BOTH, expand=tkinter.YES)
    #Create a Text widget and bind key presses
    global _tk_text_frame
    _tk_text_frame = tkinter.Text(frame1) 
    _tk_text_frame.pack(fill=tkinter.BOTH, expand=tkinter.YES)
    _tk_text_frame.bind("<Key>", key_press)
    _tk_text_frame.bind("<Return>", rtn_press)
    _tk_text_frame.edit_modified(False)
    #Create a status/message frame
    global _tk_status_frame
    _tk_status_frame = tkinter.Frame(_tk_root, relief=tkinter.RAISED, borderwidth=1)
    _tk_status_frame.pack(fill=tkinter.BOTH, expand=True)
    #Add grammar display and controls
    global _tk_grammar_frame, _tk_grammar_box
    _tk_grammar_frame = tkinter.Frame(_tk_status_frame)
    _tk_grammar_frame.pack(fill=tkinter.BOTH, expand=True)
    _tk_grammar_box = tkinter.Label(_tk_grammar_frame, text="")
    _tk_grammar_box.pack()
    global _tk_grammar_type_select
    selected_option = tkinter.StringVar()
    _tk_grammar_type_select = tkinter.ttk.Combobox(_tk_grammar_frame, textvariable=selected_option, state="readonly", values=list(_NGRAM_TYPE.keys()))
    _tk_grammar_type_select.set([key for key, value in _NGRAM_TYPE.items() if value == checkGrammar.get_current_ngram_type()][0])
    _tk_grammar_type_select.pack(side=tkinter.RIGHT, padx=5, pady=5)
    _tk_grammar_type_select.bind('<<ComboboxSelected>>', grammar_method_changed)
    next_button = tkinter.Button(_tk_grammar_frame, text="Next", command=grammar_next_selected)
    next_button.pack(side=tkinter.RIGHT, padx=5, pady=5)
    stop_button = tkinter.Button(_tk_grammar_frame, text="Stop", command=grammar_stop_selected)
    stop_button.pack(side=tkinter.RIGHT, padx=5, pady=5)
    _tk_grammar_frame.pack_forget()
    _tk_status_frame.pack_forget()
    #Add application control buttons
    global _tk_menu_frame
    _tk_menu_frame = tkinter.Frame(_tk_root)
    _tk_menu_frame.pack(fill=tkinter.BOTH, expand=True)
    quit_button = tkinter.Button(_tk_menu_frame, text="Quit", command=exit_selected)
    quit_button.pack(side=tkinter.RIGHT, padx=5, pady=5)
    global _tk_saveas_button, _tk_save_button, _tk_load_button, _tk_spelling_button, _tk_grammar_button
    _tk_saveas_button = tkinter.Button(_tk_menu_frame, text="SaveAs", command=saveas_selected)
    _tk_saveas_button.pack(side=tkinter.RIGHT, padx=5, pady=5)
    _tk_save_button = tkinter.Button(_tk_menu_frame, text="Save", command=save_selected)
    _tk_save_button.pack(side=tkinter.RIGHT, padx=5, pady=5)
    _tk_load_button = tkinter.Button(_tk_menu_frame, text="Load", command=load_selected)
    _tk_load_button.pack(side=tkinter.RIGHT, padx=5, pady=5)
    _tk_spelling_button = tkinter.Button(_tk_menu_frame, text="Spelling", command=full_spellcheck)
    _tk_spelling_button.pack(side=tkinter.LEFT, padx=5, pady=5)
    _tk_grammar_button = tkinter.Button(_tk_menu_frame, text="Grammar", command=grammar_selected)
    _tk_grammar_button.pack(side=tkinter.LEFT, padx=5, pady=5)
    #Call the event loop
    _tk_root.mainloop()

#Call the function main
main()


